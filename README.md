# About

A python-based program designed for researchers with little-to-no programming knowledge to process and analyze VigiBase/VigiLyze spreadsheets. Includes custom command support via an Excel Spreadsheet File. MIT License.

# Table of Contents

[[_TOC_]]


# Installation

## Prerequisites

1. Python v3.8 or newer

2. The following python (pip) packages
    - openpyxl
    - pandas
    - pprint
    - scipy
    - numpy

## Windows Setup

1. Install [Python v3.8 or newer](https://www.python.org/downloads/) (If you are presented with the option to add python to PATH/Environment select yes)

2. [Open Command Prompt as Administrator](https://www.google.com/search?q=open+command+prompt+as+administrator)

3. In Commmand Prompt run the following command:

    `python -m pip install openpyxl pandas pprint scipy numpy`

    - If you get an error that the command was not found you need to add python to your PATH/environment

## Linux Setup

1. Install Python v3.8 or newer using your distrobution's package manager.

2. Install the following pip modules using your distrobution's package manager: `openpyxl pandas pprint scipy numpy`
    - If your distrobution does not provide these packages run: 
    `sudo python -m pip install openpyxl pandas pprint scipy numpy`
        - This is heavily discouraged as most distrobutions package direct support for these python modules and this could lead to breaking changes when upgrading python in the future.


# Basic Usage

Run CreateReport.py (in Windows by double-clicking it). It will ask for 4 input files:

- The Vigibase Spreadsheet **(you must provide this)**

- A Commands Spreadsheet (A sample one is included in Sample Input Spreadsheets [here](https://gitlab.com/manjalyc/vigibase/-/blob/master/Sample%20Input%20Spreadsheets/Commands.xlsx))

- An Overlaying Condition Groups Spreadsheet (A sample one based on MedDRA 20.1 is included in Sample Input Spreadsheets [here](https://gitlab.com/manjalyc/vigibase/-/blob/master/Sample%20Input%20Spreadsheets/Overlaying%20Condition%20Groups.xlsx))

- A Continent Maps Spreadsheet (A sample one is included in Sample Input Spreadsheets [here](https://gitlab.com/manjalyc/vigibase/-/blob/master/Sample%20Input%20Spreadsheets/Continent%20Groups.xlsx))

It will then ask you where to save the two output files (the spreadsheet and text files)

# Advanced Usage (& the Commands Spreadsheet)

This program can be manipulated to generate select output by editing the Commands Spreadsheet. Current features include applying filters to data, getting data point summaries, one variable statistics, overlap comparison, and more.

## The General Layout of the Commands Spreadsheet

Each analysis must start with:

| Title | Your Analysis Title |
|-------|---------------------|

Multiple Analysis are supported in a single Commands File, to start a new analysis simply make a new title

## Filters

Each Analysis can define a set of parent filters. To define a set of parent filters in the row after title add a cell labeled Filters and in the subsequent rows add a filter by (Vigibase Sheetname, Vigibase ColumnName, Search string, exact matches on search string). Filters SERVE ONLY TO ELIMINATE CASE IDS THAT DO NOT MATCH THE SEARCH TERM (THEY DO NOT ELIMINATE DRUGS/REACTIONS), as such it is recommended to only filter by the Cases sheet unless you know what you are doing. Custom sheets are supported, multiple filters (and no filters are supported):

| Title   | European Male Breakdown |        |       |
|---------|-------------------------|--------|-------|
| Filters |                         |        |       |
| Sheet name  | Column name       | Search String   | Exact Matches? |
| Sheet name  | Column name       | Search String   | Exact Matches? |


Ex. 1: Filtering by Male Cases in Europe:

| Title   | European Male Breakdown |        |       |
|---------|-------------------------|--------|-------|
| Filters |                         |        |       |
| Cases   | Sex                     | Male   | Exact |
| Custom  | Continent               | Europe | Exact |

Ex. 2: Applying No Filters:

| Title   | Complete Breakdown |        |       |
|---------|-------------------------|--------|-------|
| Filters |                         |        |       |

## Spreadsheet Commands

Spreadsheet commands are use to generate the output spreadsheet. The spreadsheet commands section starts with one row with the first cell labeled "Spreadsheet Commands", the following commands are supported:

### Summarize

Give a summary of available datapoints under a sheet and column (custom sheets are supported):
Give a summary of available datapoints under a sheet and column (custom sheets are supported):

| Summarize | Sheetname | Column name | Exact Matches? |
|-----------|-------|---------------------------|-------|

Ex: Summarize distrobution of cases by country of primary source

Command:

| Summarize | Cases | Country of primary source | Exact |
|-----------|-------|---------------------------|-------|

Output:

| Summarize          | Cases                                                | Country of primary source | Exact |
|--------------------|------------------------------------------------------|---------------------------|-------|
| # Of Processed IDs | 500                                                   |                           |       |
| # Of Datapoints    | 10                                                   |                           |       |
| Are Matchs Exact?  | True                                                 |                           |       |
|                    | Country of primary source                            | Count                     |       |
|                    | Austria                                              | 1                         |       |
|                    | Belgium                                              | 2                         |       |
|                    | Czechia                                              | 3                         |       |
|                    | France                                               | 4                         |       |
|                    | Germany                                              | 5                         |       |
|                    | Italy                                                | 6                         |       |
|                    | Norway                                               | 7                         |       |
|                    | Slovakia                                             | 8                         |       |
|                    | Switzerland                                          | 9                         |       |
|                    | United Kingdom of Great Britain and Northern Ireland | 10                         |       |


### One Var Stats

Get basic statistics (mean, median, 95% CI, IQR, min, max) of a numerical column.

| One Var Stats | Sheetname | Column name | Decimal Points |
|---------------|-------|-----|---|

Command:

| One Var Stats | Cases | Age | 1 |
|---------------|-------|-----|---|

Output:

| One Var Stats      | Cases                        | Age       | 1         |
|--------------------|------------------------------|-----------|-----------|
| # Of Processed IDs | 100                          |           |           |
|                    | # Of IDs with Available Data | 92        |           |
|                    | Median [q1-q3]               | 47.5      | 32.3-58.2 |
|                    | (Sample) Mean [stdev]        | 55.2      | 14.3      |
|                    | 95% CI                       | 48.2-63.2 |           |
|                    | Range [min-max]              | 59        | 22.0-81.0 |

### Core Drug Stats

Get a breakdown of a suspect drug. Drug column name is typically either 'WHODrug active ingredient' or 'WHODrug trade name' but can be any column in the drugs spreadsheet.

Format:

| Core Drug Stats | Drug column name | Search Term | Exact |
|-----------------|---------------------------|-----------|-------|

Ex: Get Core Drug Stats by active ingredient Ponatinib

Command:

| Core Drug Stats | WHODrug active ingredient | Ponatinib | Exact |
|-----------------|---------------------------|-----------|-------|

Output:

| Core Drug Stats                             | WHODrug active ingredient                                | Ponatinib                    | Exact     |
|---------------------------------------------|----------------------------------------------------------|------------------------------|-----------|
| # Of Processed IDs                          | 100                                                      |                              |           |
|                                             | Total Cases Under Parent Filter                          | 38                           |           |
|                                             | Cases with suspect drug(s)                               | 25                           |           |
|                                             | Cases with suspect Ponatinib (WHODrug active ingredient) | 25                           |           |
|                                             | Only Ponatinib                                           | 13                           |           |
|                                             | Ponatinib + 1 other drug                                 | 7                            |           |
|                                             | Ponatinib + >=2 other drugs                              | 5                            |           |
| Max Dose of Ponatinib per Case ID (per Day) | Dose                                                     | Count                        |           |
|                                             | 1.0 DF                                                   | 1                            |           |
|                                             | 15.0 mg                                                  | 2                            |           |
|                                             | 30.0 mg                                                  | 3                            |           |
|                                             | 45.0 mg                                                  | 4                            |           |
|                                             | Unknown                                                  | 5                            |           |
|                                             |                                                          |                              |           |
| First AE Outcome after Ponatinib            | Dose                                                     | Count                        |           |
|                                             | Died                                                     | 1                            |           |
|                                             | Not recovered                                            | 2                            |           |
|                                             | Recovered                                                | 3                            |           |
|                                             | Recovered with sequelae                                  | 4                            |           |
|                                             | Recovering                                               | 5                            |           |
|                                             | Unknown                                                  | 6                            |           |
|                                             |                                                          |                              |           |
| Days between drug Ponatinib and First AE    | One Var Stats                                            | Use Text Methods To Get List |           |
|                                             | # Of IDs with Available Data                             | 92                           |           |
|                                             | Median [q1-q3]                                           | 47.5                         | 32.3-58.2 |
|                                             | (Sample) Mean [stdev]                                    | 55.2                         | 14.3      |
|                                             | 95% CI                                                   | 48.2-63.2                    |           |
|                                             | Range [min-max]                                          | 59                           | 22.0-81.0 |
|                                             |                                                          |                              |           |
|                                             | Days between drug Ponatinib and First AE IDs             | Use Text Methods To Get List |           |
|                                             | Number of Serious Cases under Ponatinib                  | 20                           |           |


### Drugs Stats with Target AE

Similar to core drug stats but instead of tracking the First AE, it tracks a target AE of interest. This target AE can either be a specific MedDra term (in sheet Reactions, column MedDRA preferred term) or a general overlaying condition as defined in the condition groups spreadsheet.

Format:

| Drug Stats With Target AE | Drug Column Name | Search Term | Exact Matches | AE Target of Interest | AE Target is group? |
|---------------------------|---------------------------|-----------|-------|--------------|---|

Ex. 1: Get Drug stats by active ingredient ponatinib to the specific AE Hypertension.

Command:

| Drug Stats With Target AE | WHODrug active ingredient | Ponatinib | Exact | Hypertension | False |
|---------------------------|---------------------------|-----------|-------|--------------|---|

Output:

| Drug Stats With Target AE                   | WHODrug active ingredient                                | Ponatinib                    | Exact     | Hypertension | 0 |
|---------------------------------------------|----------------------------------------------------------|------------------------------|-----------|--------------|---|
| # Of Processed IDs                          | 100                                                      |                              |           |              |   |
|                                             | Total Cases Under Parent Filter                          | 38                           |           |              |   |
|                                             | Cases with suspect drug(s)                               | 25                           |           |              |   |
|                                             | Cases with suspect Ponatinib (WHODrug active ingredient) | 25                           |           |              |   |
|                                             | Only Ponatinib                                           | 13                           |           |              |   |
|                                             | Ponatinib + 1 other drug                                 | 7                            |           |              |   |
|                                             | Ponatinib + >=2 other drugs                              | 5                            |           |              |   |
| Max Dose of Ponatinib per Case ID (per Day) | Dose                                                     | Count                        |           |              |   |
|                                             | 1.0 DF                                                   | 1                            |           |              |   |
|                                             | 15.0 mg                                                  | 2                            |           |              |   |
|                                             | 30.0 mg                                                  | 3                            |           |              |   |
|                                             | 45.0 mg                                                  | 4                            |           |              |   |
|                                             | Unknown                                                  | 5                            |           |              |   |
|                                             |                                                          |                              |           |              |   |
| Target AE Outcome after Ponatinib           | Dose                                                     | Count                        |           |              |   |
|                                             | Died                                                     | 1                            |           |              |   |
|                                             | Not recovered                                            | 2                            |           |              |   |
|                                             | Recovered                                                | 3                            |           |              |   |
|                                             | Recovered with sequelae                                  | 4                            |           |              |   |
|                                             | Recovering                                               | 5                            |           |              |   |
|                                             | Unknown                                                  | 6                            |           |              |   |
| Days between drug Ponatinib and Target AE   |                                                          |                              |           |              |   |
|                                             | One Var Stats                                            | Use Text Methods To Get List |           |              |   |
|                                             | # Of IDs with Available Data                             | 92                           |           |              |   |
|                                             | Median [q1-q3]                                           | 47.5                         | 32.3-58.2 |              |   |
|                                             | (Sample) Mean [stdev]                                    | 55.2                         | 14.3      |              |   |
|                                             | 95% CI                                                   | 48.2-63.2                    |           |              |   |
|                                             | Range [min-max]                                          | 59                           | 22.0-81.0 |              |   |
|                                             |                                                          |                              |           |              |   |
|                                             | Days between drug Ponatinib and First AE IDs             | Use Text Methods To Get List |           |              |   |
|                                             | Is target AE 'Hypertension' MedDRA term                  | True                         |           |              |   |
|                                             | Is target AE 'Hypertension' MedDRA group                 | False                        |           |              |   |home
|                                             | Number of Serious Cases under Ponatinib                  | 17                           |           |              |   |

Ex. 2: Get Drug stats by active ingredient ponatinib to the MedDra Group 'Central nervous system vascular disorders (SMQ)' as defined by the overlaying condition groups spreadsheet


| Drug Stats With Target AE | WHODrug active ingredient | Ponatinib | Exact | Central nervous system vascular disorders (SMQ | True |
|---------------------------|---------------------------|-----------|-------|--------------|-------|


### Group Associated Drug Stats

Get the number of cases under each AE group (as defined in the overlaying condition groups spreadsheet) that were administered a drug.

Format:

| Group Associated Drug Stats | Drug column name | Searchterm | Exact Matches? |
|-----------------------------|---------------------------|-----------|-------|

Ex: Get group associated drug stats for active ingredient ponatinib

Command:

| Group Associated Drug Stats | WHODrug active ingredient | Ponatinib | Exact |
|-----------------------------|---------------------------|-----------|-------|

Output: (Trimmed)

| # Of Processed IDs         | 100                                         |       |
|----------------------------|--------------------------------------------|-------|
| Are Matchs Exact?          | True                                       |       |
| Ponatinib Matchs Per Group | Group Name                                 | Count |
|                            | Accidents and injuries (SMQ)               | 49    |
|                            | Acute central respiratory depression (SMQ) | 44    |
|                            | Acute pancreatitis (SMQ)                   | 26   |
|                            | Acute renal failure (SMQ)                  | 30    |
|                            | Agranulocytosis (SMQ)                      | 33    |
|                            | Anaphylactic reaction (SMQ)                | 43   |
|                            | ...                           | ...    |

### Simple Overlap Count Table

Get # of IDs that overlap by sheetname/column name. Custom spreadsheets are supported. Typically used to find overlap for Reaction Groups.

Format:

| Simple Overlap Count Table | Sheetname | Column name |
|----------------------------|--------|------------------|

Ex: Overlap Table for Reactions Group

Command:

| Simple Overlap Count Table | Custom | Reactions: Group |
|----------------------------|--------|------------------|

Output: 

| Group Name                                 | Accidents and injuries (SMQ) | Acute central respiratory depression (SMQ) | Acute pancreatitis (SMQ) | Anaphylactic reaction (SMQ) | Anticholinergic syndrome (SMQ) | Arthritis (SMQ) |
|--------------------------------------------|------------------------------|--------------------------------------------|--------------------------|-----------------------------|--------------------------------|-----------------|
| Accidents and injuries (SMQ)               |                              | 1                                          | 2                        | 3                           | 4                              | 5               |
| Acute central respiratory depression (SMQ) | 1                            |                                            | 1                        | 2                           | 3                              | 4               |
| Acute pancreatitis (SMQ)                   | 2                            | 1                                          |                          | 1                           | 2                              | 3               |
| Anaphylactic reaction (SMQ)                | 3                            | 2                                          | 1                        |                             | 1                              | 2               |
| Anticholinergic syndrome (SMQ)             | 4                            | 3                                          | 2                        | 1                           |                                | 1               |
| Arthritis (SMQ)                            | 5                            | 4                                          | 3                        | 2                           | 1                              |                 |


### All Suspect Drugs Info

Get a breakdown of all suspect drugs by drug column name (typically WHODrug active ingredient)

Format:

| All Suspect Drugs Info | Drug Column Name |
|------------------------|---------------------------|

Ex:

| All Suspect Drugs Info | WHODrug active ingredient |
|------------------------|---------------------------|

Output:

| All Suspect Drugs Info    | WHODrug active ingredient |       |
|---------------------------|---------------------------|-------|
| # Of Processed IDs        | 100                       |       |
| # Of Unique Suspect Drugs | 50                        |       |
|                           | WHODrug active ingredient | Count |
|                           | Alemtuzumab               | 1     |
|                           | Amlodipine                | 2     |
|                           | Atorvastatin              | 3     |
|                           | Blinatumomab              | 4     |
|                           | Bosutinib                 | 5     |
|                           | Calcium carbonate         | 6     |
|                           | Chlorothiazide            | 7     |
|                           | Cyclophosphamide          | 8     |
|                           | Cytarabine                | 9     |
|                           | Dasatinib                 | 1     |
|                           | ...            | ...     |


## Textfile Commands

Textfile commands are use to generate the output textfile. Textfile commands tend to be more powerful than the spreadsheet commands and are used to retrieve data (such as id dumps for parent filters or available custom sheets) that are not suited to be in a spreadsheet. The textfile commands section starts with one row with the first cell labeled "Textfile Commands", the following commands are supported:


### Get Available Sheets and Columns

Retrieves all available sheet names and column names including custom sheets.

Format:

| Get Available Sheets and Columns |
|----------------------------------|

Ex:

Command:

| Get Available Sheets and Columns |
|----------------------------------|

Output:

```
Command: ['Get Available Sheets and Columns']
All Available Keys [sheetName][columnName]: 
	[Cases][Action taken with drug]
	[Cases][Age group]
	[Cases][Age unit]
	[Cases][Age]
	[Cases][Completeness score]
	[Cases][Country of primary source]
	[Cases][Dosage regimen]
	[Cases][Dose unit]
	[Cases][Dose]
	[Cases][End date.1]
	[Cases][End date]
	[Cases][Fatal]
	[Cases][Height (cm)]
	[Cases][Indication]
	[Cases][Mapped term]
	[Cases][MedDRA preferred term]
	[Cases][Number of Suspect/Interacting drugs]
	[Cases][Original format]
	[Cases][Outcome]
	[Cases][Reported medication]
	[Cases][Reporter qualification]
	[Cases][Role]
	[Cases][Route of admin.]
	[Cases][Safety report ID]
	[Cases][Serious]
	[Cases][Seriousness criteria]
	[Cases][Sex]
	[Cases][Start Date]
	[Cases][Start date]
	[Cases][Study name]
	[Cases][Suspected duplicate of UMC report ID (vigiMatch)]
	[Cases][Term reported to UMC]
	[Cases][UMC report ID]
	[Cases][VigiBase initial date]
	[Cases][VigiBase latest entry date]
	[Cases][WHODrug active ingredient]
	[Cases][WHODrug trade name]
	[Cases][Weight (kg)]
	[Cases][Worldwide unique ID]
	[Custom][Cases: VigiBase initial entry date: Year]
	[Custom][Cases: VigiBase latest entry date: Year]
	[Custom][Continent]
	[Custom][Drug Dosage (Daily)]
	[Custom][Drugs: Total Concomitant]
	[Custom][Drugs: Total Suspect]
	[Custom][Extra: (H) Drug-AE Matrix]
	[Custom][Reaction: (H) Reaction Duration Days]
	[Custom][Reactions: Group]
	[Drugs][Action taken with drug]
	[Drugs][Dosage regimen]
	[Drugs][Dose unit]
	[Drugs][Dose]
	[Drugs][End date]
	[Drugs][Indication]
	[Drugs][Reported medication]
	[Drugs][Role]
	[Drugs][Route of admin.]
	[Drugs][Safety report ID]
	[Drugs][Start date]
	[Drugs][UMC report ID]
	[Drugs][WHODrug active ingredient]
	[Drugs][WHODrug trade name]
	[Reactions][End date]
	[Reactions][Mapped term]
	[Reactions][MedDRA preferred term]
	[Reactions][Outcome]
	[Reactions][Safety report ID]
	[Reactions][Start date]
	[Reactions][Term reported to UMC]
	[Reactions][UMC report ID]
```

### Get Available Datapoints

Give a summary of available datapoints under a sheet and column (custom sheets are supported). Analogous to the spreadsheet command `Summarize`

Format:

| Get Available Datapoints | Sheetname | Column name | Exact? |
|--------------------------|-------|---------------------------|-------|

Ex: Get breakdown by country of primary sources

Command: 

| Get Available Datapoints | Cases | Country of primary source | Exact |
|--------------------------|-------|---------------------------|-------|

Output

```
Command: ['Get Available Datapoints', 'Cases', 'Country of primary source', 'Exact']
dataPoint (exact) breakdown for [Cases][Country of primary source] (Processed 28 ids) (10 dataPoints): 
	'Austria', exact matches = 1
	'Belgium', exact matches = 6
	'Czechia', exact matches = 1
	'France', exact matches = 5
	'Germany', exact matches = 2
	'Italy', exact matches = 8
	'Norway', exact matches = 1
	'Slovakia', exact matches = 2
	'Switzerland', exact matches = 1
	'United Kingdom of Great Britain and Northern Ireland', exact matches = 1
```

### Get Matches

Search a column in a sheet for number of matches to a search term and get the IDs.

Ex: Get # of Matches and IDs for male cases

Command:

| Get Matches | Sheetname | Columnname | Search Term | Exact? |
|-------------|-------|-----|------|-------|

Output:

```
Command: ['Get Matches', 'Cases', 'Sex', 'Male', 'Exact']
# of Matches in [Cases][Sex] for Male (exactMatch = True): 11
IDs of matches: {0000001, 0000002, 0000003, 0000004, 0000005, 0000006, 0000007, 0000008, 0000009, 0000010, 0000011}
```

### Get Core Drug Stats

Get a breakdown of a suspect drug. Drug column name is typically either 'WHODrug active ingredient' or 'WHODrug trade name' but can be any column in the drugs spreadsheet Analogous to Spreadsheet Command `Core Drug Stats` but also outputs IDs.


Format:

| Get Core Drug Stats | Drug column name | Search Term | Exact |
|-----------------|---------------------------|-----------|-------|

Ex: Get Core Drug Stats by active ingredient Ponatinib

Command:

| Get Core Drug Stats | WHODrug active ingredient | Ponatinib | Exact |
|-----------------|---------------------------|-----------|-------|

Output:

```
Command: ['Get Core Drug Stats', 'WHODrug active ingredient', 'Ponatinib', 'Exact']
Total Cases Under Parent Filter: 100
Cases with suspect drug(s): 100
Cases with suspect Ponatinib (WHODrug active ingredient): 98
Only Ponatinib: 18
Ponatinib + 1 other drug: 50
Ponatinib + >=2 other drugs: 20
Max Dose of Ponatinib per Case ID (per Day): {'1.0 DF': 1, '15.0 mg': 2, '30.0 mg': 3, '45.0 mg': 4, 'Unknown': 5}
First AE Outcome after Ponatinib: {'Died': 1,
 'Not recovered': 2,
 'Recovered': 3,
 'Recovered with sequelae': 4,
 'Recovering': 5,
 'Unknown': 6}
Days between drug Ponatinib and First AE: 
(0, 0, 0, 2, 2, 2, 3, 4, 5...)
	Median [IQR]: 120 [5-440]
	Mean (stdev): 300 (400)
	[min-max]: 0-1050
Days between drug Ponatinib and First AE IDs: 
(0000001, 0000002, 0000003, 0000004...)
Number of Serious Cases under Ponatinib: 17
```

### Get Drug Stats With Target AE

Similar to `get core drug stats` but instead of tracking the First AE, it tracks a target AE of interest. This target AE can either be a specific MedDra term (in sheet Reactions, column MedDRA preferred term) or a general overlaying condition as defined in the condition groups spreadsheet. Analogous to Spreadsheet Command `Drug Stats with Target AE` but also outputs IDs.

Format:

| Get Drug Stats With Target AE | Drug Column Name | Search Term | Exact Matches | AE Target of Interest | AE Target is group? |
|---------------------------|---------------------------|-----------|-------|--------------|---|

Ex. 1: Get Drug stats by active ingredient ponatinib to the specific AE Hypertension.

Command:

| Get Drug Stats With Target AE | WHODrug active ingredient | Ponatinib | Exact | Hypertension | False |
|---------------------------|---------------------------|-----------|-------|--------------|---|

Output:

```
Command: ['Get Drug Stats With Target AE', 'WHODrug active ingredient', 'Ponatinib', 'Exact', 'Hypertension', '=FALSE()']
Total Cases Under Parent Filter: 100
Cases with suspect drug(s): 100
Cases with suspect Ponatinib (WHODrug active ingredient): 98
Only Ponatinib: 18
Ponatinib + 1 other drug: 50
Ponatinib + >=2 other drugs: 20
Max Dose of Ponatinib per Case ID (per Day): {'1.0 DF': 1, '15.0 mg': 2, '30.0 mg': 3, '45.0 mg': 4, 'Unknown': 5}
First AE Outcome after Ponatinib: {'Died': 1,
 'Not recovered': 2,
 'Recovered': 3,
 'Recovered with sequelae': 4,
 'Recovering': 5,
 'Unknown': 6}
Days between drug Ponatinib and First AE: 
(0, 0, 0, 2, 2, 2, 3, 4, 5...)
	Median [IQR]: 120 [5-440]
	Mean (stdev): 300 (400)
	[min-max]: 0-1050
Days between drug Ponatinib and First AE IDs: 
(0000001, 0000002, 0000003, 0000004...)
Is target AE 'Hypertension' MedDRA term: True
Is target AE 'Hypertension' MedDRA group: False
Number of Serious Cases under Ponatinib: 17
```

### Get One Var Stats

Get basic statistics (mean, median, 95% CI, IQR, min, max) of a quantitative column. Analogous to Spreadsheet command `One Var Stats`. Note in the textfile the decimal points are ignored.

Format:

| Get One Var Stats | Sheetname | Column name | Decimal Points |
|---------------|-------|-----|---|


Ex: Get One Var Stats for the Age distrobution

Command:

| Get One Var Stats | Cases | Age | 1 |
|---------------|-------|-----|---|

Output:

```
Command: ['Get One Var Stats', 'Cases', 'Age', 1]
One Var Stats for [Cases][Age] (# Of IDs with Available Data = 22): 
	Median [IQR]: 60.5 [33.8-73.5]
	Mean (stdev): 45.90909090909091 (13.66745373724421)
	95% CI: 42.405906240441446-64.41227557774037
	[min-max]: 21.0-79.0
```

### ID Dump

Dump all IDs under the parent filter(s).

Format:

| ID Dump |
|---------|

Ex:

Command:

| ID Dump |
|---------|

Output:

```
Command: ['ID Dump']
28 Current Ids in DataBase: 0000001, 0000002, 0000003, 0000004, 0000005, 0000006, 0000007, 0000008, 0000009, 0000010, 0000011..
```

### Get Group Associated Drug Stats

Get the number of cases under each AE group (as defined in the overlaying condition groups spreadsheet) that were administered a drug. Analogous to spreadsheet command `Group Associated Drug Stats`

Format:

| Get Group Associated Drug Stats | Drug column name | Searchterm | Exact Matches? |
|-----------------------------|---------------------------|-----------|-------|

Ex: Get group associated drug stats for active ingredient ponatinib

Command:

| Group Associated Drug Stats | WHODrug active ingredient | Ponatinib | Exact |
|-----------------------------|---------------------------|-----------|-------|

Output:

```
Command: ['Get Group Associated Drug Stats', 'WHODrug active ingredient', 'Ponatinib', 'Exact']
Group Associated Case ID counts under WHODrug active ingredient for Ponatinib, exactMatches = True:
	Accidents and injuries (SMQ): 1
	Acute central respiratory depression (SMQ): 2
	Acute pancreatitis (SMQ): 3
	Acute renal failure (SMQ): 4
	Agranulocytosis (SMQ): 5
	Anaphylactic reaction (SMQ): 6
	Angioedema (SMQ): 7
	Anticholinergic syndrome (SMQ): 8
	Arthritis (SMQ): 9
	Asthma/bronchospasm (SMQ): 10
...
```

### Get Simple Overlap Count Info

Get # of IDs that overlap by sheetname/column name. Custom spreadsheets are supported. Typically used to find overlap for Reaction Groups. Analogous to spreadsheet command `Simple Overlap Count Table` (its probably better to only use the spreadsheet cinnabd due to the nature of this data).

Format:

| Get Simple Overlap Count Info | Sheetname | Column name |
|----------------------------|--------|------------------|

Ex: Overlap Table for Reactions Group

Command:

| Get Simple Overlap Count Info | Custom | Reactions: Group |
|----------------------------|--------|------------------|

Output: A JSON-formatted dict

```
Command: ['Get Simple Overlap Count Info', 'Custom', 'Reactions: Group']
Simple Group Overlap ID Counts for [Custom][Reactions: Group]: 
{'Accidents and injuries (SMQ)': {'Acute central respiratory depression (SMQ)': 1,
                                  'Acute pancreatitis (SMQ)': 2,
                                  'Anaphylactic reaction (SMQ)': 3,
                                  'Anticholinergic syndrome (SMQ)': 4, ...},
'Acute central respiratory depression (SMQ)': {'Accidents and injuries (SMQ)': 1,
                                                'Acute pancreatitis (SMQ)': 2,
                                                'Anaphylactic reaction (SMQ)': 3,
                                                'Anticholinergic syndrome (SMQ)': 4, ...},
...}
```


### Get All Suspect Drugs Info

Get a breakdown of all suspect drugs by drug column name (typically WHODrug active ingredient). Analogous to spreadsheet command `All Suspect Drugs Info`.

Format:

| Get All Suspect Drugs Info | Drug Column Name |
|------------------------|---------------------------|

Ex:

| Get All Suspect Drugs Info | WHODrug active ingredient |
|------------------------|---------------------------|

Output:

```
Command: ['Get All Suspect Drugs Info', 'WHODrug active ingredient']
Case ID Count for suspect drugs under WHODrug active ingredient:
	Alemtuzumab: 1
	Amlodipine: 2
	Atorvastatin: 3
	Blinatumomab: 4
```
