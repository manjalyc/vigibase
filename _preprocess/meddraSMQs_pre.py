#!/bin/python3
import openpyxl as op
from openpyxl.styles import Font

smq_spreadsheet = 'smq_spreadsheet_20_1_English.xlsx'
smq_version = '20.1'
condition_groups_spreadsheet = '../Sample Input Spreadsheets/Overlaying Condition Groups.xlsx'

wb = op.load_workbook(filename=smq_spreadsheet, read_only=True)

# print(wb.sheetnames)

ws = wb[f'SMQ {smq_version}']  # could also retrieve by index

smqDict = {}
currSMQ = None

# valToSmq = {}
# dups = 0

for row in ws.values:
    vals = []
    for val in row:
        vals.append(val)
    while len(vals) > 0 and vals[-1] == None: del vals[-1]
    if len(vals) == 1:
        # print(vals)
        currSMQ = vals[0]
        if currSMQ not in smqDict:
            smqDict[currSMQ] = []
    else:
        # if vals[-2] == None: print(vals)
        if currSMQ and vals[-2] != None:
            smqDict[currSMQ].append(vals[-2])
            """
            if vals[-2] in valToSmq:
                dups += 1
                print('ex', vals[-2], '|swt|', valToSmq[vals[-2]], '|remap>|', currSMQ)
            valToSmq[vals[-2]] = currSMQ
            """

wb.close()

# Write Condition Groups Sheet
wb = op.Workbook()
ws = wb.active
ws.title = f'Generated-SMQ-v{smq_version}'
col = 1
for smq in sorted(smqDict.keys()):
    header = ws.cell(column=col, row=1, value=smq)
    header.font = Font(bold=True)
    row = 2
    for AE in smqDict[smq]:
        ws.cell(column=col, row=row, value=AE)
        row += 1
        # print(smq, AE)
    col += 1
wb.save(condition_groups_spreadsheet)
wb.close()
# print('dups', dups)


"""
import pprint
for key in smqDict:
    print(key)
    pprint.pprint(smqDict[key])
"""
