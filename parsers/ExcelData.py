#Requires the pandas and openpyxl python modules to be installed to read excel files
import pandas as pd

#For disk caching operation (optimize regen times as slowdown identified by cProfile)
#Note Experimental
import os
global diskCache, nan
nan = None
diskCache = './Cache'
import codecs
import datetime

#Identify stdout hijack and go mute
global _stdout
import sys
_stdout = sys.stdout


from datetime import date
def deltaDays(dateString1, dateString2):
	assumeStartDay = False
	assumeEndDay = False
	if pd.isnull(dateString1) or pd.isnull(dateString2): return None
	#iNM = 0
	
	#print(dateString1, dateString2)
	#Less Precise Heuristic, But Includes more data
	if dateString1.count('-') == 0: return None #dateString1 += '-01-01'
	elif dateString1.count('-') == 1:
		dateString1 += '-15'
		assumeStartDay = True

	if dateString2.count('-') == 0: return None #dateString2 += '-01-01'
	elif dateString2.count('-') == 1:
		dateString2 += '-15'
		assumeEndDay = True
		#iNM = 1

	if assumeStartDay and assumeEndDay: return None
	
	y1, m1, d1 = map(int, dateString1.split('-'))
	y2, m2, d2 = map(int, dateString2.split('-'))
	
	#m2 += iNM
	if m2 > 12:
		y2 += 1
		m2 -= 12
	
	delta = date(y2,m2,d2) - date(y1,m1,d1)
	
	dayDiff = delta.days# - iNM
	#print(date(y2,m2,d2), date(y1,m1,d1), delta.days)

	if dayDiff < 0:
		if assumeStartDay or assumeEndDay: dayDiff += 15

	return dayDiff
	
class ExcelData:
	#Internal Methods, Meant to Be Used for Processing
	excelFile = ''
	continentsMapFile = None
	conditionsMapFile = None
	idDict = {}
	useCache = False
	diskIDCache = ''
	
	def __init__(self, excelFileLoc, continentMapSpreadSheet = None, conditionsGroupSpreadSheet = None, useCache = False):
		self.excelFile = excelFileLoc
		self.idDict = {} #Mapped by UMC report ID, Submapped by Cases, Drugs, and Reactions
		self.continentsMapFile = continentMapSpreadSheet
		self.conditionsMapFile = conditionsGroupSpreadSheet
		
		self.genIdDict()
		
		self.useCache = useCache
		self.diskCacheFile = f"{diskCache}/cachedDict-{os.getpid()}.tmp"
		if useCache: self.setCache()

	def setCache(self):
		global _stdout
		global diskCache
		if _stdout == sys.stdout: print(f"[CACHE][INST]: {self.diskCacheFile}")
		if not os.path.exists(diskCache): os.mkdir(diskCache)

		"""
                #Windows Can't Handle This due to utf issues
		with open(self.diskCacheFile, 'w') as dcc:
			dcc.write(str(self.idDict))
		"""
		with codecs.open(self.diskCacheFile, 'w', 'utf-8') as dcc:
			dcc.write(str(self.idDict))

	def getCache(self, printStatus = False):
		if _stdout == sys.stdout and printStatus: print(f"[CACHE][PULL]: {self.diskCacheFile}")

		"""
                #Windows Can't Handle This due to utf issues
		self.idDict = eval(open(self.diskCacheFile, 'r').read())
		"""

		self.idDict = eval(codecs.open(self.diskCacheFile, 'r', 'utf-8').read())

	def getSimpleMap(self, mapFile):
		dF = pd.read_excel(mapFile, engine = 'openpyxl')
		conMap = {}
		for column in dF.columns:
			header = column
			#print(continent)
			for val in dF[column]:
				if not pd.isnull(val): conMap[val] = header
		return conMap

	def getMultiRevMap(self, mapFile):
		dF = pd.read_excel(mapFile, engine = 'openpyxl')
		revMap = dict()
		for column in dF.columns:
			head = column
			#print(continent)
			for key in dF[column]:
				if not pd.isnull(key):
					if key not in revMap: revMap[key] = set()
					revMap[key].add(head)
		return revMap


	#Generate [ID]['Custom']['Continent']
	def mapContinents(self):
		if not self.continentsMapFile: return "No Continents File Specified"
		conMap = self.getSimpleMap(self.continentsMapFile)

		#import pprint
		#pprint.pprint(conMap)

		for ID in self.idDict:
			country = self.idDict[ID]['Cases']['Country of primary source']
			self.idDict[ID]['Custom']['Continent'] = conMap.get(country, f"Unknown: '{country}' | Please add to {self.continentsMapFile}" )

	#Generate [ID]['Custom']['Reactions - conditionsGroupSpreadSheet']
	def mapConditions(self):
		if not self.conditionsMapFile: return "No Conditions File Specified"
		revMap = self.getMultiRevMap(self.conditionsMapFile)

		for ID in self.idDict:
			conditions = self.idDict[ID]['Reactions']['MedDRA preferred term']
			self.idDict[ID]['Custom']['Reactions: Group'] = []
			for i in range(len(conditions)):
				condition = conditions[i] #ensure ordering
				if condition not in revMap:
					self.idDict[ID]['Custom']['Reactions: Group'].append('Uncategorized MedDRA terms')
					#print('Uncategorized MedDRA Term: ', condition)
					continue
				for head in revMap[condition]:
					self.idDict[ID]['Custom']['Reactions: Group'].append(head)

	def EDdeltaDays(date1, date2):
		return deltaDays(date1, date2)
	
	#rows are by drugIdx, columns are by reactionIdx
	def getMatrixDrugToAEDuration(self, drugDict, reactionDict):
		reactionDates = reactionDict['Start date']
		drugDates = drugDict['Start date']
		mat = []
		for dIdx in range(len(drugDates)):
			drugAE_row = []
			for rIdx in range(len(reactionDates)): drugAE_row.append(deltaDays(drugDates[dIdx], reactionDates[rIdx]))
			mat.append( drugAE_row )
		return mat
	
	def genCustomDataPoints(self):
		todel = []
		for ID in self.idDict:
			#Cases: VigiBase initial date: Year
			#Cases: VigiBase initial date: Year
			#if pd.isnull(ID): continue

			#if keyerror on Cases, check ID
			#print(ID, self.idDict[ID])
			if 'Cases' not in self.idDict[ID]:
				print(f"[SUSPECT][!!!] ID: {ID} not in Cases Sheet, deleting this ID...")
				todel.append(ID)
				continue
			if 'Drugs' not in self.idDict[ID]:
				print(f"[SUSPECT][!!!] ID: {ID} not in Drugs Sheet, deleting this ID...")
				todel.append(ID)
				continue
			if 'Reactions' not in self.idDict[ID]:
				print(f"[SUSPECT][!!!] ID: {ID} not in Reactions Sheet, deleting this ID...")
				todel.append(ID)
				continue

			self.idDict[ID]['Custom'] = {}
			self.idDict[ID]['Custom']['Cases: VigiBase initial entry date: Year'] = str(self.idDict[ID]['Cases']['VigiBase initial date'])[:4]
			self.idDict[ID]['Custom']['Cases: VigiBase latest entry date: Year'] = str(self.idDict[ID]['Cases']['VigiBase latest entry date'])[:4]
			
			#Reaction: (H) Reaction Duration Days
			reactionDurationList = []
			for i in range(len(self.idDict[ID]['Reactions']['Start date'])):
				startDate = self.idDict[ID]['Reactions']['Start date'][i]
				endDate = self.idDict[ID]['Reactions']['End date'][i]
				duration = None
				duration = deltaDays(startDate, endDate)
				reactionDurationList.append(duration)
				if type(duration) == int and duration < 0: print(ID, startDate, endDate)
			self.idDict[ID]['Custom']['Reaction: (H) Reaction Duration Days'] = reactionDurationList
			
			#Extra: (H) Matrix, Time to AE from Drug
			self.idDict[ID]['Custom']['Extra: (H) Drug-AE Matrix'] = self.getMatrixDrugToAEDuration(self.idDict[ID]['Drugs'], self.idDict[ID]['Reactions'])
			
			#Number of Suspect/Concomitant Drugs per id
			nS = 0
			nC = 0
			nI = 0
			for i in range(len(self.idDict[ID]['Drugs']['Role'])):
				if self.idDict[ID]['Drugs']['Role'][i] == 'Suspect': nS += 1
				elif self.idDict[ID]['Drugs']['Role'][i] == 'Interacting': nI += 1
				elif self.idDict[ID]['Drugs']['Role'][i] == 'Concomitant': nC += 1
			self.idDict[ID]['Custom']['Drugs: Total Suspect'] = nS
			self.idDict[ID]['Custom']['Drugs: Total Interacting'] = nI
			self.idDict[ID]['Custom']['Drugs: Total Concomitant'] = nC
			
			#Dosage Amount Per Day
			dosesPerDay = []
			
			for i in range(len(self.idDict[ID]['Drugs']['Dose'])):
				dosePerDay = self.idDict[ID]['Drugs']['Dose'][i]
				doseFreq = self.idDict[ID]['Drugs']['Dosage regimen'][i]
				
				#Redundant
				#doseUnit = self.idDict[ID]['Drugs']['Dose unit'][i]
				#doseUnits.append(doseUnit)
				
				if (pd.isnull(doseFreq)
				or doseFreq.lower() == '1 per 1 day'
				or doseFreq.lower() == '‒ per 1 day'
				or doseFreq.lower() == '- per 1 day'):
					dosesPerDay.append(dosePerDay)
					continue
				
				doseFreq = doseFreq.replace('‒ ‒','1 day')
				doseFreq = doseFreq.replace('‒','1')
				doseFreq = doseFreq.replace('days','day')
				doseFreq = doseFreq.replace('weeks','week')
				doseFreq = doseFreq.replace('hours','hour')
				doseFreq = doseFreq.replace('months','month')
				doseFreq = list(filter(str.lower, doseFreq.split()))
				if ('hour' in doseFreq
					or 'day' in doseFreq
					or 'week' in doseFreq
					or 'month' in doseFreq):
					
					#print(doseFreq)
					dosePerDay = dosePerDay*float(doseFreq[0])
					if doseFreq[-1] == 'day':
						dosePerDay = dosePerDay*float(doseFreq[-2])
					elif doseFreq[-1] == 'hour':
						dosePerDay = dosePerDay*float(doseFreq[-2])*24
					elif doseFreq[-1] == 'week':
						dosePerDay = dosePerDay*float(doseFreq[-2])/7
					elif doseFreq[-1] == 'month':
						dosePerDay = dosePerDay*float(doseFreq[-2])/30 #assumed 30?
					dosesPerDay.append(dosePerDay)
					continue
				
				#Ideally this part is never reached, but if it is something is wrong
				dosesPerDay.append(f"{dosePerDay} (unparseable @{ID})")
			
			self.idDict[ID]['Custom']['Drug Dosage (Daily)'] = dosesPerDay
			#print(self.idDict[ID]['Custom']['Drug Dosage (Daily)'])
		for ID in todel:
			del self.idDict[ID]
		#Custom Mappings
		self.mapContinents()
		self.mapConditions()
	
	def addSheetToIdDict(self, sheetName):
		dF = pd.read_excel(self.excelFile, sheetName, engine = 'openpyxl')
		
		for r_idx, row in dF.iterrows():
			uj = row['UMC report ID']
			if pd.isnull(uj): continue
			key = int(uj)
			if pd.isnull(key): continue
			if key not in self.idDict: self.idDict[key] = {}
			if sheetName not in self.idDict[key]: self.idDict[key][sheetName] = {}
			for colName, data in row.iteritems():
				colName = str(colName)
				#print(colName)
				if sheetName != 'Cases':
					if colName not in self.idDict[key][sheetName]: self.idDict[key][sheetName][colName] = []
					self.idDict[key][sheetName][colName].append(data)
				elif colName not in self.idDict[key][sheetName]:
					self.idDict[key][sheetName][colName] = data
				else:
					raise ValueError(f"Duplicate Values for [{key}][{sheetName}][{colName}]")
	
	def genIdDict(self):
		del self.idDict
		self.idDict = {}
		#print('Cases')
		self.addSheetToIdDict('Cases')
		#print('Drugs')
		self.addSheetToIdDict('Drugs')
		#print('Reactions')
		self.addSheetToIdDict('Reactions')
		#self.printIdDictKeys()
		self.genCustomDataPoints()
	
	def getSubDataPoints(self, dataPoint):
		if type(dataPoint) == list: return dataPoint
		return str(dataPoint).split('\n')
	
	def getDataPoints(self, sheetName, colName, forceIgnoreCase = True):
		dataPoints = set()
		uniqueLower = set()
		for ID in self.idDict:
			for dataPoint in self.getSubDataPoints(self.idDict[ID][sheetName][colName]):
				if forceIgnoreCase:
					if str(dataPoint).lower() in uniqueLower: continue
					else: uniqueLower.add(str(dataPoint).lower())
				dataPoints.add(str(dataPoint))
		return sorted(dataPoints)

	def getMatches(self, sheetName, colName, searchTerm, exact = False):
		matchIDs = set()
		for ID in self.idDict:
			for dataPoint in self.getSubDataPoints(self.idDict[ID][sheetName][colName]):
				dataPoint = str(dataPoint)
				if exact and str(searchTerm).lower() == str(dataPoint).lower():
					matchIDs.add(ID)
					break #don't double count a cell for a match
				elif not exact and str(searchTerm).lower() in str(self.idDict[ID][sheetName][colName]).lower():
					matchIDs.add(ID)
					break #don't double count a cell for a match
		return matchIDs
	
	def getNumMatches(self, sheetName, colName, searchTerm, exact = False):
		return len(self.getMatches(sheetName, colName, searchTerm, exact))


	#Front-End Methods (For Filtering/Resetting Data)
	def reGenIdDict(self):
		if not self.useCache: self.genIdDict()
		else: self.getCache()
		return f"The DataBase has been reinitialized to contain all {len(self.idDict)} ids"
	
	def filterIDs(self, sheetName, colName, searchTerm, exact = False):
		if exact == True: return self.filterIDsExact(sheetName, colName, searchTerm)
	
		matchIDs = self.getMatches(sheetName, colName, searchTerm, False)
		for ID in list(self.idDict.keys()):
			if ID not in matchIDs: del self.idDict[ID]
		return f"The DataBase has been (loose) filtered and now only contains: {len(self.idDict)} ids which had an match with '{searchTerm}' in [{sheetName}][{colName}]"
	
	def filterIDsExact(self, sheetName, colName, searchTerm):
		matchIDs = self.getMatches(sheetName, colName, searchTerm, True)
		for ID in list(self.idDict.keys()):
			if ID not in matchIDs: del self.idDict[ID]
		return f"The DataBase has been (exact) filtered and now only contains: {len(self.idDict)} ids which had an match with '{searchTerm}' in [{sheetName}][{colName}]"


	def getIDs(self):
		ret = set()
		for ID in self.idDict: ret.add(ID)
		return ret

	def filterByIDs(self, idIterator):
		for ID in list(self.idDict.keys()):
			if ID not in idIterator: del self.idDict[ID]
		return f"The DataBase has been filtered according to an idList, and now only contains: {len(self.idDict)} ids"


	#Print Methods, meant to be used for Reporting Data
	def printOrderedSet(self, mySet, prepend = ""):
		#print(mySet)
		sList = sorted(mySet)
		for identifier in sList:
			print(f"{prepend}{identifier}")

	def printCurrentIds(self):
		IDs = list(sorted(self.idDict.keys()))
		print(f"{len(IDs)} Current Ids in DataBase:", ', '.join( map(str, IDs) ))
	
	def printIdDictKeys(self):
		print("All Available Keys [sheetName][columnName]: ")
		accessible = set()
		for ID in self.idDict:
			for sheetName in self.idDict[ID]:
				for colName in self.idDict[ID][sheetName]:
					accessible.add(f"[{sheetName}][{colName}]")
					#print(f"[{sheetName}][{colName}]")
		self.printOrderedSet(accessible,"\t")
	
	def printDataPoints(self, sheetName, colName):
		print(f"Available (exact) dataPoints for [{sheetName}][{colName}]: ")
		self.printOrderedSet(self.getDataPoints(sheetName, colName),"\t")
	
	def printDataPointSummary(self, sheetName, colName, exact = False):
		if exact: return self.printDataPointSummaryExact(sheetName, colName)
		dataPoints = self.getDataPoints(sheetName, colName)
		print(f"dataPoint (loose) breakdown for [{sheetName}][{colName}] (Processed {len(self.idDict)} ids) ({len(dataPoints)} dataPoints): ")
		for dataPoint in dataPoints:
			print(f"\t'{dataPoint}', loose matches = {self.getNumMatches(sheetName, colName, dataPoint)}")
	
	def printDataPointSummaryExact(self, sheetName, colName):
		dataPoints = self.getDataPoints(sheetName, colName)
		print(f"dataPoint (exact) breakdown for [{sheetName}][{colName}] (Processed {len(self.idDict)} ids) ({len(dataPoints)} dataPoints): ")
		for dataPoint in dataPoints:
			print(f"\t'{dataPoint}', exact matches = {self.getNumMatches(sheetName, colName, dataPoint, True)}")
	
	def printMatches(self, sheetName, colName, searchTerm, exactMatch):
		matches = self.getMatches(sheetName, colName, searchTerm, exact = exactMatch)
		print(f"# of Matches in [{sheetName}][{colName}] for {searchTerm} (exactMatch = {exactMatch}):{len(matches)}")
		print(f"IDs of matches: {matches}")
	
	def printNumMatches(self, sheetName, colName, searchTerm):
		print(f"Searched {len(self.idDict)} ids under [{sheetName}][{colName}] for '{searchTerm}', loose matches = {self.getNumMatches(sheetName, colName, searchTerm)}")
	
	def printNumMatchesExact(self, sheetName, colName, searchTerm):
		print(f"Searched {len(self.idDict)} ids under [{sheetName}][{colName}] for '{searchTerm}', exact matches = {self.getNumMatchesExact(sheetName, colName, searchTerm, True)}")

def main():
	#Useful For Testing
	#eD = ExcelData('Documents/Hypertension - Take 425 - Copy.xlsx', useCache = True)
	
	excelFile = '../Sample Input Spreadsheets/Hypertension - Take 425 - Copy.xlsx'
	continentMapsExcelFile = '../Sample Input Spreadsheets/Continent Groups.xlsx'
	conditionGroups = '../Sample Input Spreadsheets/Overlaying Condition Groups.xlsx'
	
	eD = ExcelData(excelFile,
				continentMapSpreadSheet = continentMapsExcelFile, conditionsGroupSpreadSheet = conditionGroups,
				useCache = True)
	
	eD.printIdDictKeys()
	
	import pprint
	pp = pprint.PrettyPrinter(indent=2)
	
	"""
	print(eD.idDict[29990214]['Drugs']['Dosage regimen'])
	print(type(eD.idDict[29990214]['Drugs']['Dosage regimen']))
	print(pd.isnull(eD.idDict[29990214]['Drugs']['Dosage regimen']))
	print(len(eD.idDict))
	"""
	
	#eD.printNumMatches('Cases','Country of primary source','United States of America')
	#eD.printDataPoints('Cases','Country of primary source')
	#eD.printDataPointSummary('Cases','Country of primary source')
	#eD.printDataPointSummary('Cases','Mapped term')
	#eD.printDataPointSummaryExact('Cases','Mapped term')
	

	#print(eD.idDict[29909133]['Drugs'])
	#print(eD.idDict[30018496]['Reactions'])
	#eD.printDataPointSummary('Cases','Sex')
	#eD.printDataPointSummaryExact('Cases','Sex')
	#eD.printDataPointSummaryExact('Reactions','Mapped term')
	#print(eD.idDict[19645876]['Reactions']['Mapped term'])
	
	#eD.printDataPointSummaryExact('Cases', 'VigiBase initial date')
	#eD.printDataPointSummaryExact('Custom', 'Cases: VigiBase initial entry date: Year')
	#eD.printDataPointSummaryExact('Custom', 'Cases: VigiBase latest entry date: Year')
	#eD.printDataPointSummaryExact('Cases','Completeness score')
	#eD.printDataPointSummaryExact('Cases','Reporter qualification')
	#eD.printDataPointSummaryExact('Cases','Age')
	

	#eD.printDataPointSummaryExact('Reactions','Start date')
	#eD.printDataPointSummaryExact('Reactions','End date')
	#eD.printDataPointSummaryExact('Reactions','Outcome')
	#eD.printDataPointSummaryExact('Custom','Reaction: (H) Reaction Duration Days')
	
	#print(eD.idDict[30018496]['Custom']['Extra: (H) Drug-AE Matrix']) #2 days to both adverse events
	#print(eD.idDict[29406035]['Custom']['Extra: (H) Drug-AE Matrix'])
	#eD.printDataPointSummaryExact('Drugs', 'WHODrug trade name')
	
	#print(eD.filterIDs('Cases','Sex','Male')) 
	#print(eD.filterIDsExact('Cases','Sex','Male')) 
	#eD.reGenIdDict()
	

	#eD.printDataPointSummaryExact('Cases', 'MedDRA preferred term')
	#eD.dumpDataPoints('Cases', 'MedDRA preferred term')
	#eD.printDataPointSummaryExact('Reactions', 'Outcome')
	#eD.printDataPointSummaryExact('Reactions', 'MedDRA preferred term')
	
	#eD.printCurrentIds()
	
	#pp.pprint(eD.idDict[29990214])
	
	#Cache Test
	
	"""
	eD.printDataPointSummaryExact('Cases','Sex')
	#print(eD.filterIDsExact('Cases','Sex','Male'))
	eD.printDataPointSummaryExact('Cases','Sex')
	eD.reGenIdDict()
	eD.printDataPointSummaryExact('Cases','Sex')
	"""

if __name__ == '__main__':
    main()
