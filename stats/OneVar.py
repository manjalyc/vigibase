import statistics as stat
global _stdout
import sys
import numpy as np
import scipy.stats
import traceback
_stdout = sys.stdout

#Current Implementation: https://docs.python.org/3/library/statistics.html

#Measures of Central Location
def mean(data):
	if len(data) == 0: return None
	return stat.mean(data)

def fmean(data):
	if len(data) == 0: return None
	return stat.fmean(data)

def median(data):
	if len(data) == 0: return None
	return stat.median(data)

def median_low(data):
	if len(data) == 0: return None
	return stat.median_low(data)

def median_high(data):
	if len(data) == 0: return None
	return stat.median_high(data)

def mode(data):
	if len(data) == 0: return None
	return stat.mode(data)

def multimode(data):
	if len(data) == 0: return None
	return stat.multimode(data)

#Measures of Spread
#population
def pstdev(data):
	data = list(data)
	if len(data) == 1: return None
	elif len(data) == 0: return None
	return stat.pstdev(data)

def pvariance(data):
	data = list(data)
	if len(data) == 1: return None
	elif len(data) == 0: return None
	return stat.pvariance(data)

#sample
def stdev(data):
	data = list(data)
	if len(data) == 1: return None
	elif len(data) == 0: return None
	return stat.stdev(data)

def variance(data):
	data = list(data)
	if len(data) == 1: return None
	elif len(data) == 0: return None
	return stat.variance(data)

#Quartiles
def quartiles(data):
	#Requires python v3.8 or higher due to quantiles
	data = list(data)
	if len(data) == 1: return (None, data[0], None)
	elif len(data) == 0: return (None, None, None)
	try:
		q1, median, q3 = stat.quantiles(data, n = 4, method = 'exclusive')
		return (q1, median, q3)
	except Exception as e:
		sys.stdout = _stdout
		message = f"[CAUGHT][NOTIF]: This Program Requires Utility only in Python v3.8 and newer"
		version = f"Current Version: {sys.version}"
		n = max(len(version), len(message))
		print("!"*n)
		print(f"[CAUGHT][NOTIF]: Exception Intercept")
		print(message)
		print(version)
		print(f"Please install python v3.8 or newer and reinstall any necessary modules...")
		print(f"If python v3.8 or newer is installed, ensure it is in the default path")
		print(f"Bailing For Traceback...")
		print("!"*n)
		#print(traceback.format_exc())
		raise e

def outliersByIQR(data):
	q1, median, q3 = quartiles(data)
	lower_bound = q1 - 1.5*(q3 - q1)
	upper_bound = q3 + 1.5*(q3 - q1)
	outliers = []
	for val in data:
		if val < lower_bound: outliers.append(val)
		elif val > upper_bound: outliers.append(val)
	return outliers

def meanConfidenceInterval(data, confidence = 0.95):
	da = np.array(data)
	n = len(da)
	mean = np.mean(da)
	serr = scipy.stats.sem(da)
	delt = serr * scipy.stats.t.ppf((1 + confidence)/2.0, n-1)
	return mean-delt, mean+delt
