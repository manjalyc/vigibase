import parsers.VigibaseFunctions as VF
from parsers.VigibaseFunctions import isExact
from parsers.ExcelData import ExcelData


from datetime import datetime

#Hijack Stdout Redirection/Restoration
global _stdout
import sys
_stdout = sys.stdout

class OutputTxt:
	tL = None
	eD = None
	filters = None
	old_stdout = None
	fO = None
	title = None
	appendToFile = False

	def __init__(self, txtLocation, vigibase_ExcelData, filters = None, title = None, appendToFile = False):
		self.tL = txtLocation
		self.eD = vigibase_ExcelData
		self.title = title
		self.appendToFile = appendToFile
		self.filters = filters
		
		self.initFile()
	
	def initFile(self):
		
		if self.appendToFile: self.fO = open(self.tL, 'a')
		else:
			self.fO = open(self.tL, 'w')
		dt = datetime.now()
		dt = dt.strftime("%H:%M:%S on %Y-%m-%d")
		
		u = max(len(f"Title: {self.title}"), len(f"Generated at {dt}"))
		self.appendLineToFile("="*u)
		if self.title != None:
			self.appendLineToFile(f"Title: {self.title}")
		self.appendLineToFile(f"Generated at {dt}")
		self.appendLineToFile("="*u)
		self.setFilters(self.filters)
	
	def closeFile(self):
		self.fO.close()
		
	def close(self):
		self.appendLineToFile('\n')
		self.closeFile()
	
	def setFilters(self, new_filters):
		self.filters = new_filters
		self.applyFilters()
		self.printFilters()
	
	def printFilters(self):
		if self.filters == None: return
		self.forceStdOutToFile()
		print()
		t = f"The Following Parent Set Filters Are Now In Use ({len(self.eD.idDict)} IDs): "
		print('=' * len(t))
		print(t)
		for filt in self.filters:
			print(f"\t{filt}")
		print('=' * len(t))
		self.restoreStdOut()
	
	def applyFilters(self): VF.filterData(self.eD, self.filters, printStatus = False)
	
	def forceStdOutToFile(self):
		sys.stdout = self.fO
		return
	
	def restoreStdOut(self):
		global _stdout
		sys.stdout = _stdout
	
	def testStdOut(self):
		self.forceStdOutToFile()
		print('TEST')
		self.restoreStdOut()
		print('TEST F')
	
	def appendLineToFile(self, line, newline = True):
		if newline: self.fO.write(f"{str(line)}\n")
		else: self.fO.write(str(line))
	
	def commandInit(self, commandList):
		self.applyFilters()
		while commandList[-1] == None: del commandList[-1]
		print(f"[TNOTI][COMM]: {commandList}")
		self.forceStdOutToFile()
		print()
		t = f"Command: {commandList}"
		print('=' * len(t))
		print(t)
	
	def termCommand(self):
		self.restoreStdOut()
	
	#info methods are for text file creation (for getting more advanced information)
	def availableSheetsAndColumns(self, commandList):
		self.commandInit(commandList)
		self.eD.printIdDictKeys()
		self.termCommand()
		return
	
	def availableDatapoints(self, commandList):
		self.commandInit(commandList)
		
		sheetName = commandList[1]
		colName = commandList[2]
		exactMatch = isExact(commandList[3])
		self.eD.printDataPointSummary(sheetName, colName, exact = exactMatch)
		
		self.restoreStdOut()
		return
	
	def availableMatches(self, commandList):
		self.commandInit(commandList)
		
		sheetName = commandList[1]
		colName = commandList[2]
		searchTerm = commandList[3]
		exactMatch = isExact(commandList[4])
		self.eD.printMatches(sheetName, colName, searchTerm, exactMatch)
		
		self.termCommand()
		return
	
	def coreDrugStats(self, commandList):
		self.commandInit(commandList)
		
		drugColName = commandList[1]
		drugOrIngredient = commandList[2]
		exactMatch = isExact(commandList[3])
		VF.printDrugStats(self.eD, drugColName, drugOrIngredient, exactM = exactMatch, filters = self.filters)
		
		self.termCommand()
		return
	
	def targetAEDrugStats(self, commandList):
		self.commandInit(commandList)
		
		drugColName = commandList[1]
		drugOrIngredient = commandList[2]
		exactMatch = isExact(commandList[3])
		target = commandList[4]
		targetIsGroup = 'true' in str(commandList[5]).lower()
		VF.printDrugStatsToTargetAE(self.eD, drugColName, drugOrIngredient, target, targetIsGroup, exactM = exactMatch, filters = self.filters)
		
		self.termCommand()
		return

	def idDump(self, commandList):
		self.commandInit(commandList)
		
		self.eD.printCurrentIds()
		
		self.termCommand()
		return
	
	def oneVarStats(self, commandList):
		self.commandInit(commandList)
		
		sheetName = commandList[1]
		colName = commandList[2]
		VF.printOneVarStats(self.eD, sheetName, colName)
		
		self.termCommand()
		return
	
	def groupDrugStats(self, commandList):
		self.commandInit(commandList)
		
		drugColName = commandList[1]
		drugOrIngredient = commandList[2]
		exactMatch = isExact(commandList[3])
		VF.printGroupAssociatedDrugStats(self.eD, drugColName, drugOrIngredient, exactM = exactMatch, filters = self.filters)
		
		self.termCommand()
		return
	
	def overlapInfo(self, commandList):
		self.commandInit(commandList)
		
		sheetName = commandList[1]
		colName = commandList[2]
		VF.printSimpleGroupOverlaps(self.eD, sheetName, colName, filters = self.filters)
			
			
		self.termCommand()
		return
	
	def suspectDrugInfo(self, commandList):
		self.commandInit(commandList)
		
		drugColName = commandList[1]
		VF.printSuspectDrugCounts(self.eD, drugColName, filters = self.filters)
		
		self.termCommand()
		return
