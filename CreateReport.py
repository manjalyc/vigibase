#!/usr/bin/python3

# Windows
# Please run 'pip install openpyxl pandas pprint numpy scipy' as administrator in command prompt

# Linux
# Use distrobution methods to install openpyxl, pandas, numpy, scipy & pprint for python3, (sudo) pip install should be used as a last restort

# Custom Imports
from parsers.ExcelData import ExcelData
import parsers.VigibaseFunctions as VF
from parsers.VigibaseFunctions import isExact
from writers.TxtWriter import OutputTxt
from writers.ExcelWriter import OutputSheet

# For Specifying the Location of the Excel Files
import tkinter as tk
from tkinter import filedialog, messagebox
import os.path

# For Reading Excel Files
from openpyxl import load_workbook as lw

# Hijack Stdout Redirection/Restoration
global _stdout
import sys

_stdout = sys.stdout

def getRowVals(row):
    vals = []
    for val in row:
        vals.append(val)
    while len(vals) > 0 and vals[-1] == None: del vals[-1]
    if len(vals) == 0: vals.append('')
    return vals

def getFilter(vals):
    # print("gF: ", vals, isExact(vals[3]))
    if isExact(vals[3]): return (vals[0], vals[1], vals[2], True)
    return (vals[0], vals[1], vals[2], False)

def executeCommands(eD, ws, ssLoc='./Output/out.xlsx', txtLoc='./Output/out.txt'):
    title = ''
    filters = []
    currMode = 'Unknown'
    OT = None
    OS = None
    appendTxt = False
    # OT.testStdOut()
    #i = 0
    for row in ws.values:
        vals = getRowVals(row)
        #i += 1
        #print('row', i, vals)
        if vals[0] == '':
            continue
        elif vals[0] == None:
            continue
        elif vals[0] == 'Title':
            title = vals[1]
            filters = []
            currMode = 'Unknown'
            print(f"\n[NOTIF][NEXT]: Processing {title}...")
        elif vals[0] == 'Spreadsheet Commands':
            currMode = 'ss'
            OS = OutputSheet(ssLoc, eD, title, filters=filters)
        elif vals[0] == 'Textfile Commands':
            currMode = 'txt'
            OT = OutputTxt(txtLoc, eD, filters=filters, title=title, appendToFile=appendTxt)
            appendTxt = True
        elif vals[0] == 'Filters':
            currMode = 'Filters'
        elif currMode == 'Filters':
            filters.append(getFilter(vals))
        elif currMode == 'ss':
            if vals[0] == 'Summarize':
                OS.summarize(vals)
            elif vals[0] == 'One Var Stats':
                OS.oneVarStats(vals)
            elif vals[0] == 'All Suspect Drugs Info':
                OS.allSuspectDrugs(vals)
            elif vals[0] == 'Core Drug Stats':
                OS.coreDrugStats(vals)
            elif vals[0] == 'Drug Stats With Target AE':
                OS.targetAEDrugStats(vals)
            elif vals[0] == 'Group Associated Drug Stats':
                OS.groupDrugStats(vals)
            elif vals[0] == 'Simple Overlap Count Table':
                OS.genOverlapTable(vals)
            else:
                print(f"[!!!!!][WARN] No Spreadsheet Commands method available for {vals} in ")
        elif currMode == 'txt':
            if vals[0] == 'Get Available Sheets and Columns':
                OT.availableSheetsAndColumns(vals)
            elif vals[0] == 'Get Available Datapoints':
                OT.availableDatapoints(vals)
            elif vals[0] == 'Get Matches':
                OT.availableMatches(vals)
            elif vals[0] == 'Get Core Drug Stats':
                OT.coreDrugStats(vals)
            elif vals[0] == 'Get One Var Stats':
                OT.oneVarStats(vals)
            elif vals[0] == 'ID Dump':
                OT.idDump(vals)
            elif vals[0] == 'Get Group Associated Drug Stats':
                OT.groupDrugStats(vals)
            elif vals[0] == 'Get Simple Overlap Count Info':
                OT.overlapInfo(vals)
            elif vals[0] == 'Get All Suspect Drugs Info':
                OT.suspectDrugInfo(vals)
            elif vals[0] == 'Get Drug Stats With Target AE':
                OT.targetAEDrugStats(vals)
            elif vals[0] == None:
                continue
            else:
                print(f"[!!!!!][WARN] Unknown InfoFile Command, skipped: {vals}")
    if OT: OT.close()
    if OS: OS.close()

# interface methods for file dialogs
def getLastUsedInCache(printStatus = True):
    ret = []
    if printStatus: print("[CACHE][PULL]: ./Cache/lastUsedFiles.txt")
    for line in open('./Cache/lastUsedFiles.txt', 'r'):
        ret.append(line.strip())
    return ret

def putLastUsedInCache(vigibase_file, continent_file, conditions_file, commands_file, outputTxtLoc, outputSpreadsheetLoc):
    print("[CACHE][INST]: ./Cache/lastUsedFiles.txt")
    with open('./Cache/lastUsedFiles.txt', 'w') as o:
        o.write(f"{vigibase_file}\n")
        o.write(f"{continent_file}\n")
        o.write(f"{conditions_file}\n")
        o.write(f"{commands_file}\n")
        o.write(f"{outputTxtLoc}\n")
        o.write(f"{outputSpreadsheetLoc}\n")
    return

def promptLastUsedCache():
    mg_root = tk.Tk()
    if not os.path.isfile('./Cache/lastUsedFiles.txt'): return False

    vigibase_file, continent_file, conditions_file, commands_file, outputTxtLoc, outputSpreadsheetLoc = getLastUsedInCache()

    u = messagebox.askyesno(title="CreateReport.py",
                            message=f"Use last file locations (Will Overwrite Existing Output)?\n\nVigibase File Location:\n{vigibase_file}\n\nCommands File Location:\n{commands_file}\n\nCondition Groups File Location:\n{conditions_file}\n\nContinent Groups File Location:\n{continent_file}\n\nOutput Text File Location:\n{outputTxtLoc}\n\nOutput Spreadsheet File Location:\n{outputSpreadsheetLoc}")
    mg_root.destroy()
    if u: return True
    return False

def getSpreadSheetLocations():
    root = tk.Tk()
    root.withdraw()
    print('Please Select the Vigibase Excel File (*.xlsx)')
    vigibase_file = filedialog.askopenfilename(
        title='Open Vigibase Excel File',
        filetypes=[('Excel File', '*.xlsx')],
        defaultextension='*.xlsx')

    print('Please Select the Commands Excel File (*.xlsx)')
    commands_file = filedialog.askopenfilename(
        title='Open Commands Excel File',
        filetypes=[('Excel File', '*.xlsx')],
        defaultextension='*.xlsx')

    print('Please Select the Condition Groups Excel File (*.xlsx)')
    conditions_file = filedialog.askopenfilename(
        title='Open Conditions Group Excel File',
        filetypes=[('Excel File', '*.xlsx')],
        defaultextension='*.xlsx')

    print('Please Select the Continent Groups Excel File (*.xlsx)')
    continent_file = filedialog.askopenfilename(
        title='Open Continent Groups Excel File',
        filetypes=[('Excel File', '*.xlsx')],
        defaultextension='*.xlsx')
    root.destroy()

    return (vigibase_file, continent_file, conditions_file, commands_file)

def getOutputLocations():
    root = tk.Tk()
    root.withdraw()

    print('Where should the output text file be saved?')
    outputTxtLoc = filedialog.asksaveasfilename(filetypes=[('Text Document', '*.txt')], defaultextension='*.txt',
                                                title='Save Output Textfile As')

    print('Where should the output spreadsheet file be saved?')
    outputSpreadsheetLoc = filedialog.asksaveasfilename(filetypes=[('Excel File', '*.xlsx')], defaultextension='*.xlsx',
                                                        title='Save Output Spreadsheet As')

    root.destroy()
    return (outputTxtLoc, outputSpreadsheetLoc)

def main():
    # print()
    vigibase_file = None
    continent_file = None
    conditions_file = None
    commands_file = None
    outputTxtLoc = None
    outputSpreadsheetLoc = None

    useLast = promptLastUsedCache()

    if not useLast:
        vigibase_file, continent_file, conditions_file, commands_file = getSpreadSheetLocations()
        outputTxtLoc, outputSpreadsheetLoc = getOutputLocations()
    else:
        vigibase_file, continent_file, conditions_file, commands_file, outputTxtLoc, outputSpreadsheetLoc = getLastUsedInCache()

    if not os.path.exists('Cache'): os.makedirs('Cache')
    print()
    print(f'[SELECT][INFO] Vigibase File Location: {vigibase_file}')
    print(f'[SELECT][INFO] Commands File Location: {commands_file}')
    print(f'[SELECT][INFO] Condition Groups File Location: {conditions_file}')
    print(f'[SELECT][INFO] Continent Groups File Location: {continent_file}')
    print(f'[SELECT][INFO] Output Text File Location: {outputTxtLoc}')
    print(f'[SELECT][INFO] Output Spreadsheet File Location: {outputSpreadsheetLoc}')
    print()

    putLastUsedInCache(
        vigibase_file,
        continent_file,
        conditions_file,
        commands_file,
        outputTxtLoc,
        outputSpreadsheetLoc
    )

    eD = ExcelData(
        excelFileLoc=vigibase_file,
        continentMapSpreadSheet=continent_file,
        conditionsGroupSpreadSheet=conditions_file,
        useCache=True)

    #eD.printDataPointSummaryExact('Reactions',  'MedDRA preferred term')

    # VF.printDrugStatsToTargetAE(eD, 'WHODrug active ingredient', 'Ponatinib', 'Hypertension', False, exactM = True, filters = None)

    # VF.printDrugStatsToTargetAE(eD, 'WHODrug active ingredient', 'Ponatinib', 'A Words', True, exactM = True, filters = None)

    # VF.printDrugStats(eD, 'WHODrug active ingredient', 'Ponatinib', exactM = True, filters = None)
    wb = lw(filename=commands_file, read_only=True)
    ws = wb.active
    executeCommands(eD, ws, ssLoc=outputSpreadsheetLoc, txtLoc=outputTxtLoc)

import traceback
if __name__ == '__main__':
    try:
        main()
        # Cleanup
        excelDataCacheFile = f"./Cache/cachedDict-{os.getpid()}.tmp"
        if os.path.isfile(excelDataCacheFile): os.remove(excelDataCacheFile)
    except Exception as e:
        sys.stdout = _stdout
        print("[Exception Caught][v0.8] Printing Traceback:")
        print(traceback.format_exc())
    input("Press Enter to exit...")
