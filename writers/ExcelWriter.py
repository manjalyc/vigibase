from parsers.ExcelData import ExcelData
import parsers.VigibaseFunctions as VF
from parsers.VigibaseFunctions import isExact

from datetime import datetime
import sys

from openpyxl import Workbook
#Basic Formatting
from openpyxl.styles import Font, Border, Side, colors, fills
#Conditional Formatting
from openpyxl.formatting.rule import ColorScaleRule
from openpyxl.formatting.rule import ColorScale, FormatObject
from openpyxl.styles import Color
from openpyxl.utils.cell import get_column_letter

class OutputSheet:
    sL = ''
    eD = None
    filters = None
    currRow = 1
    currSheet = ''
    wb = Workbook()
    ws = None

    rgb_cyan = '90FFF0'
    rgb_orange = 'FFB03B'

    def __init__(self, sheetLocation, vigibase_ExcelData, sheetName, filters = None):
        self.sL = sheetLocation
        self.eD = vigibase_ExcelData
        self.filters = filters
        self.currSheet = sheetName

        self.initSheet()

    def initSheet(self):
        self.ws = self.wb.create_sheet(self.currSheet, -1)
        self.appendRow(['Title',self.currSheet], bold = True)

        dt = datetime.now()
        self.appendRow(['Generated', dt.strftime("%Y-%m-%d"), dt.strftime("%H:%M:%S")], bold = True)
        self.appendRow()

        self.appendRow(['Total Parent Filters', len(self.filters)], bold = True, ns_borders = True, rgb = 'ff7b5a')
        for filt in self.filters:
            filt = list(filt)
            filt[-1] = str(filt[-1])
            self.appendRow(filt, rgb = 'ffa896')
        self.appendRow()

        return

    def appendRow(self, vals = [], bold = False, rgb = None, borders = False, ns_borders = False):
        for col in range(len(vals)):
            c = self.ws.cell(row=self.currRow, column = col + 1, value = vals[col])
            if vals[col] == None: continue
            if bold:
                c.font = Font(bold = True)
            if borders:
                c.border = Border(bottom=Side(style='thin'),
                      top=Side(style='thin'),
                      left=Side(style='thin'),
                      right=Side(style='thin'))
            if ns_borders:
                c.border = Border(bottom=Side(style='thin'),
                      top=Side(style='thin'))
            if rgb != None:
                color = colors.Color(rgb = rgb)
                c.fill = fills.PatternFill(patternType='solid', fgColor = color)
        self.currRow += 1

    def close(self):
        self.wb.save(self.sL)
        return

    def setFilters(self, new_filters): self.filters = new_filters

    def applyFilters(self):
        VF.filterData(self.eD, self.filters, printStatus = False)
        #import pprint as pp
        #pp.pprint(self.eD.idDict[2178682])

    def initCommand(self, commandList):
        self.appendRow(commandList, bold = True, rgb='aed094', ns_borders = True)
        while commandList[-1] == None: del commandList[-1]
        print(f"[SNOTI][COMM]: {commandList}")
        #commandList.append(None) #In case of legacy isExact Methods
        self.applyFilters()

    def termCommand(self):
        self.appendRow()

    def summarize(self, commandList):
        self.initCommand(commandList)

        sheetName = commandList[1]
        colName = commandList[2]
        exact = isExact(commandList[3])

        dataPoints = self.eD.getDataPoints(sheetName, colName)
        self.appendRow(['# Of Processed IDs',len(self.eD.idDict)])
        self.appendRow(['# Of Datapoints',len(dataPoints)])
        self.appendRow(['Are Matchs Exact?', str(VF.isExact(exact))])

        self.appendRow([None, colName, 'Count'] , rgb = self.rgb_cyan, borders = True)
        sR = self.currRow
        for dataPoint in dataPoints:
            numMatches = self.eD.getNumMatches(sheetName, colName, dataPoint, exact = exact)
            self.appendRow([None, dataPoint, numMatches], borders = True)


        self.applyColorScale(sR, sR+len(dataPoints),3,3)
        self.termCommand()
        return

    def appendOneVarStats(self, median, q1, q3, mean, stdev, mini, maxi, size, lowerCIb, upperCIb, decimalPoints = 1):
        dP = decimalPoints
        #print(median, q1, q3, mean, stdev, mini, maxi, size, lowerCIb, upperCIb)

        if (median == None):
            self.appendRow([None, "# Of IDs with Available Data", 0], borders = True)
            return

        median = round(float(median), dP)
        if q1: q1 = round(float(q1), dP)
        if q3: q3 = round(float(q3), dP)
        mean = round(float(mean), dP)
        if stdev: stdev = round(float(stdev), dP)
        mini = round(float(mini), dP)
        maxi = round(float(maxi), dP)
        if size: size = round(float(size), dP)
        if lowerCIb: lowerCIb = round(float(lowerCIb), dP)
        if upperCIb:upperCIb = round(float(upperCIb), dP)
        self.appendRow([None, "# Of IDs with Available Data", size], borders = True)
        self.appendRow([None, "Median [q1-q3]", median,f"{q1}-{q3}"], borders = True)
        self.appendRow([None, "(Sample) Mean [stdev]", mean, stdev], borders = True)
        self.appendRow([None, "95% CI", f"{lowerCIb}-{upperCIb}"], borders = True)
        self.appendRow([None, "Range [min-max]", round((float(maxi)-float(mini)), dP), f"{mini}-{maxi}"], borders = True)

    def oneVarStats(self, commandList):
        self.initCommand(commandList)
        self.appendRow(['# Of Processed IDs',len(self.eD.idDict)])

        sheetName = commandList[1]
        colName = commandList[2]
        decimalPoints = commandList[3]
        dP = f"%.{decimalPoints}f"

        median, q1, q3, mean, stdev, mini, maxi, size, lowerCIb, upperCIb = VF.getOneVarStats(self.eD, sheetName, colName)

        self.appendOneVarStats(median, q1, q3, mean, stdev, mini, maxi, size, lowerCIb, upperCIb, decimalPoints = decimalPoints)

        self.termCommand()
        return

    #drug/ingredient based methods
    def allSuspectDrugs(self, commandList):
        self.initCommand(commandList)

        drugColName = commandList[1]


        suspectDrugCounts = VF.getSuspectDrugs(self.eD, drugColName, filters = self.filters)
        self.appendRow(['# Of Processed IDs',len(self.eD.idDict)])
        self.appendRow(['# Of Unique Suspect Drugs',len(suspectDrugCounts)])


        self.appendRow([None, drugColName, 'Count'] , rgb = self.rgb_cyan, borders = True)

        sR = self.currRow
        for drug in sorted(suspectDrugCounts.keys()):
            self.appendRow([None, drug, suspectDrugCounts[drug]], borders = True)

        self.applyColorScale(sR, self.currRow - 1, 3, 3)

        self.termCommand()
        return

    def coreDrugStats(self, commandList):
        self.initCommand(commandList)
        self.appendRow(['# Of Processed IDs',len(self.eD.idDict)])

        drugColName = commandList[1]
        drugOrIngredient = commandList[2]
        exact = isExact(commandList[3])

        statDict = VF.getDrugStats(self.eD, drugColName, drugOrIngredient, exactM = exact, filters = self.filters)



        def appendBasicKey(di, key):
            self.appendRow([None, str(key), str(di[key])], borders = True)
        for key in statDict:
            if 'Max Dose of' in key or 'First AE Outcome after' in key:
                if 'Max Dose of' in key: self.appendRow([key, 'Dose', 'Count'], borders = True, rgb = self.rgb_cyan )
                elif 'First AE Outcome after' in key: self.appendRow([key, 'Outcome', 'Count'], borders = True, rgb = self.rgb_cyan )
                statDict[key].pop(None, None)
                for nxtkey in sorted(statDict[key].keys()):
                    appendBasicKey(statDict[key], nxtkey)
                self.appendRow()
            elif 'Days between' in key:
                if 'IDs' in key: self.appendRow([None, key, "Use Text Methods To Get List"], borders=True)
                else:
                    median, q1, q3, mean, stdev, mini, maxi, size, lowerCIb, upperCIb = VF.oneVarStats(statDict[key])
                    self.appendRow([key, "One Var Stats", "Use Text Methods To Get List"], borders=True, rgb = self.rgb_cyan)
                    self.appendOneVarStats(median, q1, q3, mean, stdev, mini, maxi, size, lowerCIb, upperCIb, decimalPoints = 1)
                    self.appendRow()

            else: appendBasicKey(statDict, key)

        self.termCommand()
        return

    def targetAEDrugStats(self, commandList):
        self.initCommand(commandList)
        self.appendRow(['# Of Processed IDs',len(self.eD.idDict)])

        drugColName = commandList[1]
        drugOrIngredient = commandList[2]
        exactMatch = commandList[3]
        target = commandList[4]
        targetIsGroup = 'true' in str(commandList[5]).lower()
        statDict = VF.getDrugStatsToTargetAE(self.eD, drugColName, drugOrIngredient, target, targetIsGroup, exactM = exactMatch, filters = self.filters)
        def appendBasicKey(di, key):
            self.appendRow([None, str(key), str(di[key])], borders = True)
        for key in statDict:
            if 'Max Dose of' in key or 'Target AE Outcome after' in key:
                if 'Max Dose of' in key: self.appendRow([key, 'Dose', 'Count'], borders = True, rgb = self.rgb_cyan )
                elif 'First AE Outcome after' in key: self.appendRow([key, 'Outcome', 'Count'], borders = True, rgb = self.rgb_cyan )
                statDict[key].pop(None, None)
                for nxtkey in sorted(statDict[key].keys()):
                    appendBasicKey(statDict[key], nxtkey)
                self.appendRow()
            elif 'Days between' in key:
                if 'IDs' in key: self.appendRow([None, key, "Use Text Methods To Get List"], borders=True)
                else:
                    median, q1, q3, mean, stdev, mini, maxi, size, lowerCIb, upperCIb = VF.oneVarStats(statDict[key])
                    self.appendRow([key, "One Var Stats", "Use Text Methods To Get List"], borders=True, rgb = self.rgb_cyan)
                    self.appendOneVarStats(median, q1, q3, mean, stdev, mini, maxi, size, lowerCIb, upperCIb, decimalPoints=1)
                    self.appendRow()

            else: appendBasicKey(statDict, key)

        self.termCommand()
        return

    def groupDrugStats(self, commandList):
        #TODO: Force exactmatch to false?
        self.initCommand(commandList)
        self.appendRow(['# Of Processed IDs', len(self.eD.idDict)])

        drugColName = commandList[1]
        drugOrIngredient = commandList[2]
        exactMatch = isExact(commandList[3])
        self.appendRow(['Are Matchs Exact?', str(VF.isExact(exactMatch))])

        groupCounts = VF.getGroupAssociatedDrugStats(self.eD, drugColName, drugOrIngredient, exactM = exactMatch, filters = self.filters, printStatus = False)

        self.appendRow([f"{drugOrIngredient} Matchs Per Group", "Group Name", "Count"], borders=True, rgb = self.rgb_cyan)
        sR = self.currRow
        for groupName in sorted(groupCounts.keys()):
            self.appendRow([None, groupName, groupCounts[groupName]], borders=True)

        self.applyColorScale(sR, self.currRow - 1, 3, 3)

        self.termCommand()
        return

    #overlap methods
    def genOverlapTable(self, commandList):
        self.initCommand(commandList)
        self.appendRow(['# Of Processed IDs',len(self.eD.idDict)])


        sheetName = commandList[1]
        colName = commandList[2]

        overlapDict = VF.getSimpleGroupOverlaps(self.eD, sheetName, colName, filters = self.filters, printStatus = False)
        keys = list(sorted(overlapDict.keys()))

        keys.insert(0, "Group Name")
        keys.insert(0, None)
        self.appendRow(keys, borders = True, rgb=self.rgb_orange)
        del keys[0]
        del keys[0]

        startDataRow = self.currRow
        startDataCol = 3
        dMin = sys.maxsize
        dMax = - dMin - 1
        for i in range(0, len(keys)):
            gN1 = keys[i]
            row = [None, gN1]
            for j in range(0, len(keys)):
                gN2 = keys[j]
                if gN1 == gN2:
                    row.append(None)
                    continue
                row.append(overlapDict[gN1][gN2])
                dMin = min(dMin, overlapDict[gN1][gN2])
                dMax = max(dMax, overlapDict[gN1][gN2])
            self.appendRow(row, borders = True)
            c = self.ws.cell(row=self.currRow-1, column = 2, value = gN1)
            color = colors.Color(rgb = self.rgb_orange)
            c.fill = fills.PatternFill(patternType='solid', fgColor = color)
            #self.appendRow(keys, borders = True, rgb=self.rgb_orange)
        endDataRow = self.currRow -1
        endDataCol = startDataCol + len(keys) - 1

        self.applyColorScale(startDataRow, endDataRow, startDataCol, endDataCol)
        """
        cs_rule = ColorScaleRule(start_type = 'num', start_value = dMin , start_color=colors.WHITE,
                                 end_type='num', end_value = dMax, end_color=colors.RED)


        startDataColLet = get_column_letter(startDataCol)
        endDataColLet = get_column_letter(endDataCol)
        for rowIdx in range(startDataRow, endDataRow):
            sR = f"{startDataColLet}{rowIdx}"
            eR = f"{endDataColLet}{rowIdx}"
            self.ws.conditional_endDataRow = self.currRow
        endDataCol = startDataCol + len(keys)

        cs_rule = ColorScaleRule(start_type = 'num', start_value = dMin , start_color=colors.WHITE,
                                 end_type='num', end_value = dMax, end_color=colors.RED)


        startDataColLet = get_column_letter(startDataCol)
        endDataColLet = get_column_letter(endDataCol)
        for rowIdx in range(startDataRow, endDataRow):
            sR = f"{startDataColLet}{rowIdx}"
            eR = f"{endDataColLet}{rowIdx}"
            self.ws.conditional_formatting.add(f"{sR}:{eR}", cs_rule)
            #print(f"{sR}:{eR}")formatting.add(f"{sR}:{eR}", cs_rule)
            #print(f"{sR}:{eR}")
        """

        self.termCommand()
        return

    #Conditional Formatting Methods
    #Inclusive
    def applyColorScale(self, startRow, endRow, startCol, endCol, start_color = colors.WHITE, end_color=colors.RED):
        dMin = sys.maxsize
        dMax = - dMin - 1
        for i in range(startRow, endRow+1):
            for j in range(startCol, endCol+1):
                #print(i, j)
                j = get_column_letter(j)
                val = self.ws[f"{j}{i}"].value
                if val == None: continue
                val = float(val)
                dMax = max(dMax, val)
                dMin = min(dMin, val)

        cs_rule = ColorScaleRule(start_type='num', start_value=dMin, start_color=colors.WHITE,
                                 end_type='num', end_value=dMax, end_color=colors.RED)
        startColLet = get_column_letter(startCol)
        endColLet = get_column_letter(endCol)
        #print(f"{startColLet}{startRow}:{endColLet}{endRow}")
        self.ws.conditional_formatting.add(f"{startColLet}{startRow}:{endColLet}{endRow}", cs_rule)

