# Standard Library Imports
import itertools as it
from operator import itemgetter

# Nonstandard Library Imports
import pprint as pp
import pandas as pd

# Custom Script Imports
from parsers.ExcelData import ExcelData
import stats.OneVar as ov
import stats.compare as comp


# MISC UTILITY METHODS
def isExact(term):
    if type(term) == bool: return term
    if str(term).lower() == 'exact':
        # print(term, True)
        return True
    # print(term, False)
    return False

def filterData(eD, filters, printStatus=True, regen = True):
    if regen: eD.reGenIdDict()

    if filters != None:
        if printStatus: print("[FILTR][INFO]: Applying Following Filters: ")
    else:
        if printStatus: print("[FILTR][INFO]: No Parent Filter Applied")
        return
    for filt in filters:
        #Cases - Use Default Filtering Methods
        if filt[0] == 'Cases':
            if printStatus: print(f"\t{filt}")
            sheetName, colName, searchTerm, exactO = filt
            eD.filterIDs(sheetName, colName, searchTerm, exact=isExact(exactO))

        #Drugs - Use Custom Filtering
        #Currently only filters by Drug Name (WHODrug active ingredient) and ( and drops all other drugs
        elif filt[0] == 'Drugs':
            #print('\n','\n', filt, filt[0] == 'Drugs')
            drugColName = filt[1]
            searchTerm = filt[2]
            exact = isExact(filt[3])
            delIDs = []
            for ID in eD.idDict:
                #print('\n',ID)
                #pp.pprint(eD.idDict[ID]['Drugs'][drugColName])
                toDel = []
                delID = True
                for i in range(len(eD.idDict[ID]['Drugs'][drugColName])):
                    target = eD.idDict[ID]['Drugs'][drugColName][i]
                    if target == None:
                        toDel.append(i)
                        continue
                    if exact and target.lower() == searchTerm.lower():
                        delID = False
                        continue
                    elif not exact and searchTerm.lower() in target.lower():
                        delID = False
                        continue
                    toDel.append(i)
                for idx in toDel:
                    eD.idDict[ID]['Custom']['Drug Dosage (Daily)'][idx] = None
                    for drugCol in eD.idDict[ID]['Drugs']: eD.idDict[ID]['Drugs'][drugCol][idx] = None
                if delID: delIDs.append(ID)
            #print('del', delIDs)
            for ID in delIDs:
                del eD.idDict[ID]
                pp.pprint(eD.idDict[ID]['Drugs'][drugColName])

        #Custom Filters
        elif filt[0] == 'Custom' and filt[1] == 'Drug Dosage (Daily)':
            delIDs = []
            searchTerm = float(filt[2])
            for ID in eD.idDict:
                #print('\n',ID)
                #pp.pprint(eD.idDict[ID]['Drugs'][drugColName])
                toDel = []
                delID = True
                for i in range(len(eD.idDict[ID]['Custom']['Drug Dosage (Daily)'])):
                    target = eD.idDict[ID]['Custom']['Drug Dosage (Daily)'][i]
                    if target == None or 'unparseable' in str(target):
                        toDel.append(i)
                        continue
                    if float(target) == searchTerm:
                        delID = False
                        continue
                    toDel.append(i)
                for idx in toDel:
                    eD.idDict[ID]['Custom']['Drug Dosage (Daily)'][idx] = None
                    for drugCol in eD.idDict[ID]['Drugs']: eD.idDict[ID]['Drugs'][drugCol][idx] = None
                if delID: delIDs.append(ID)
            for ID in delIDs:
                del eD.idDict[ID]
                #pp.pprint(eD.idDict[ID]['Drugs'][drugColName])
        else:
            if printStatus: print(f"\t{filt}")
            sheetName, colName, searchTerm, exactO = filt
            eD.filterIDs(sheetName, colName, searchTerm, exact=isExact(exactO))


def oneVarStats(data, confidence=0.95):
    if len(data) == 0:
        return (None, None, None, None, None, None, None, None, None, None)
    q1, median, q3 = ov.quartiles(data)
    mean = ov.mean(data)
    stdev = ov.stdev(data)
    lowerCIb, upperCIb = ov.meanConfidenceInterval(data, confidence=confidence)
    return (median, q1, q3, mean, stdev, min(data), max(data), len(data), lowerCIb, upperCIb)

# DATA PROCESSSING METHODS
def getOneVarStats(eD, sheetName, colName):
    data = []
    for ID in eD.idDict:
        value = eD.idDict[ID][sheetName][colName]
        if not pd.isnull(value): data.append(float(value))
    return oneVarStats(data)

def isTargetAE(eD, ID, AEIdx, target, targetIsGroup):
    term = eD.idDict[ID]['Reactions']['MedDRA preferred term'][AEIdx]
    """
    print('isTargetAE', 
       target, 
       targetIsGroup, 
       term.lower() == target.lower(), 
       target.lower() == eD.idDict[ID]['Custom']['Reactions: Group'][AEIdx].lower(), 
       eD.idDict[ID]['Custom']['Reactions: Group'][AEIdx].lower(), 
       term)
    """
    #print('t', targetIsGroup, targetIsGroup == True, target)
    if targetIsGroup == False:
        return term.lower() == target.lower()
    else:
        return target.lower() == eD.idDict[ID]['Custom']['Reactions: Group'][AEIdx].lower()
    return False

def getInitialDrugAdministrationIdx(eD, ID, drugIdxs):
    if len(drugIdxs) == 0: return None
    earliestDrugIdx = drugIdxs[0]  # drugIdx
    for drugIdx in drugIdxs:
        eIdate = eD.idDict[ID]['Drugs']['Start date'][earliestDrugIdx]
        cIdate = eD.idDict[ID]['Drugs']['Start date'][drugIdx]
        if cIdate == None:
            continue
        if eIdate == None:  # Should never be the case, left here for legacy purposes
            earliestDrugIdx = drugIdx
            continue
        # print(cIdate, eIdate)
        delta = ExcelData.EDdeltaDays(eIdate, cIdate)
        if not delta: continue
        if delta < 0: earliestDrugIdx = drugIdx
    if earliestDrugIdx == -1: return None
    return earliestDrugIdx

def getDrugIdxs(eD, ID, drugColName, drug):
    drugIdxs = []
    for i in range(len(eD.idDict[ID]['Drugs'][drugColName])):
        #print(ID, drugColName, eD.idDict[ID]['Drugs'][drugColName][i])
        if eD.idDict[ID]['Drugs'][drugColName][i] == None: continue
        if eD.idDict[ID]['Drugs'][drugColName][i].lower() == drug.lower():
            drugIdxs.append(i)
    return drugIdxs

def getMaxDoseIdx(eD, ID, drugIdxs, target=None, targetIsGroup=None):
    maxDoseIdx = None
    maxDose = None
    for drugIdx in drugIdxs:
        currDose = eD.idDict[ID]['Custom']['Drug Dosage (Daily)'][drugIdx]
        """
        if target != None:
            if targetIsGroup:
                if target not in eD.idDict[ID]['Custom']['Reactions: Group']: continue
            else:
                if target.lower() != eD.idDict[ID]['Reactions']['MedDRA preferred term']: continue
        """
        if pd.isnull(currDose): continue
        if pd.isnull(maxDose):
            maxDose = currDose
            maxDoseIdx = drugIdx
            continue
        if currDose > maxDose:
            maxDoseIdx = drugIdx
            maxDose = currDose
    #print(ID, maxDoseIdx, drugIdxs)
    return maxDoseIdx

def getAEIdxsAfterDrugAdministration(eD, ID, earliestDrugIdx, target=None, targetIsGroup=None):
    AEIdxs = []
    if earliestDrugIdx == None: return AEIdxs
    for AEIdx in range(len(eD.idDict[ID]['Custom']['Extra: (H) Drug-AE Matrix'][earliestDrugIdx])):
        if target != None:
            #print('u', target)
            if not isTargetAE(eD, ID, AEIdx, target, targetIsGroup): continue
        time = eD.idDict[ID]['Custom']['Extra: (H) Drug-AE Matrix'][earliestDrugIdx][AEIdx]
        if not pd.isnull(time) and time >= 0:
            AEIdxs.append(AEIdx)
    return AEIdxs

def getSimpleGroupOverlaps(eD, sheetName, colName, filters=None, printStatus=False):
    filterData(eD, filters, printStatus=printStatus)
    groupIDs = {}
    for ID in eD.idDict:
        for group in eD.idDict[ID][sheetName][colName]:
            if group not in groupIDs: groupIDs[group] = set()
            groupIDs[group].add(ID)
    for group in groupIDs:
        groupIDs[group] = list(sorted(groupIDs[group]))

    # print(groupIDs)

    overlaps = {}
    for gN1, gN2 in it.combinations(groupIDs.keys(), 2):
        matches = comp.compareOrderedLists(groupIDs[gN1], groupIDs[gN2])
        if gN1 not in overlaps: overlaps[gN1] = {}
        if gN2 not in overlaps: overlaps[gN2] = {}
        overlaps[gN1][gN2] = len(matches)
        overlaps[gN2][gN1] = len(matches)

    return overlaps

def getDrugStats(eD, colName, drugOrIngredient, exactM=True, filters=None, printStatus=False):
    drug = drugOrIngredient
    statDict = {}

    # Parent Filter
    filterData(eD, filters, printStatus=printStatus)
    statDict['Total Cases Under Parent Filter'] = len(eD.idDict)

    # Filter Remaining To Cases with Suspect & Interacting Drugs Only
    suspectIDs = set(eD.getMatches('Drugs', 'Role', 'Suspect'))
    interactingIDs = set(eD.getMatches('Drugs', 'Role', 'Interacting'))
    suspectAndInteractingIDs = suspectIDs.union(interactingIDs)
    eD.filterByIDs(suspectAndInteractingIDs)
    statDict['Cases with suspect and/or interacting drug(s)'] = len(eD.idDict)

    # Filter Remaining To Only Containing Drug Of Interest
    eD.filterIDs('Drugs', colName, drug, exact=exactM)
    statDict[f'Cases with suspect and/or interacting {drug} ({colName})'] = len(eD.idDict)
    statDict[f'Only {drug}'] = 0
    statDict[f'{drug} + 1 other drug'] = 0
    statDict[f'{drug} + >=2 other drugs'] = 0
    statDict[f'Max Dose of {drug} per Case ID (per Day)'] = {}
    statDict[f'Max Dose of {drug} per Case ID (per Day)']['Unknown'] = 0
    statDict[f'First AE Outcome after {drug}'] = {}
    statDict[f'Days between drug {drug} and First AE'] = []
    statDict[f'Days between drug {drug} and First AE IDs'] = []
    statDict[f"Number of Serious Cases under {drug}"] = 0

    for ID in eD.idDict:
        if str(eD.idDict[ID]['Cases']['Serious']).lower() == 'yes':
            statDict[f"Number of Serious Cases under {drug}"] += 1

        # Determine Number of Suspect Drugs Per Case ID
        numSuspect = eD.idDict[ID]['Custom']['Drugs: Total Suspect']
        numInteracting = eD.idDict[ID]['Custom']['Drugs: Total Interacting']
        numSI = numSuspect + numInteracting
        #if numInteracting > 0: print('nI ;', ID, numInteracting)
        if numSI == 1:
            statDict[f'Only {drug}'] += 1
        elif numSI == 2:
            statDict[f'{drug} + 1 other drug'] += 1
        elif numSI > 2:
            statDict[f'{drug} + >=2 other drugs'] += 1

        # Get Relevant Drug Indexes
        drugIdxs = getDrugIdxs(eD, ID, colName, drug)

        # Get Maximum Dosage Per Day For This ID
        maxDoseIdx = getMaxDoseIdx(eD, ID, drugIdxs)
        if pd.isnull(maxDoseIdx):
            statDict[f'Max Dose of {drug} per Case ID (per Day)']['Unknown'] += 1
        else:
            maxDosage = f"{eD.idDict[ID]['Custom']['Drug Dosage (Daily)'][maxDoseIdx]}"
            maxDosage += f" {eD.idDict[ID]['Drugs']['Dose unit'][maxDoseIdx]}"
            if maxDosage not in statDict[f'Max Dose of {drug} per Case ID (per Day)']:
                statDict[f'Max Dose of {drug} per Case ID (per Day)'][maxDosage] = 0
            statDict[f'Max Dose of {drug} per Case ID (per Day)'][maxDosage] += 1

        # Get Initial Drug Administration --> Earliest AE & outcome after Administration
        # Issue: Multiple AEs on a single day, AEs that occur after and have different resolutions (should all AEs be included, currently on first AE)
        earliestDrugIdx = getInitialDrugAdministrationIdx(eD, ID, drugIdxs)
        outcome = None
        if earliestDrugIdx != None:
            # Get Time to First AE from First Administration of Drug
            timeToFirstAE = None
            AEIdxs = getAEIdxsAfterDrugAdministration(eD, ID, earliestDrugIdx)

            # Supersede Death for All Outcomes as the only applicable outcome
            # print('Died' in eD.idDict[ID]['Reactions']['Outcome'], eD.idDict[ID]['Reactions']['Outcome'])
            if 'Died' in eD.idDict[ID]['Reactions']['Outcome']:
                AEIdxs = [eD.idDict[ID]['Reactions']['Outcome'].index('Died')]

            for AEIdx in AEIdxs:
                time = eD.idDict[ID]['Custom']['Extra: (H) Drug-AE Matrix'][earliestDrugIdx][AEIdx]
                if timeToFirstAE == None:
                    timeToFirstAE = time
                    outcome = eD.idDict[ID]['Reactions']['Outcome'][AEIdx]
                else:
                    if time < timeToFirstAE:
                        timeToFirstAE = time
                        outcome = eD.idDict[ID]['Reactions']['Outcome'][AEIdx]

            if not pd.isnull(timeToFirstAE) and timeToFirstAE >= 0:
                statDict[f'Days between drug {drug} and First AE'].append(timeToFirstAE)
                statDict[f'Days between drug {drug} and First AE IDs'].append(ID)
                if outcome not in statDict[f'First AE Outcome after {drug}']:
                    statDict[f'First AE Outcome after {drug}'][outcome] = 0
                statDict[f'First AE Outcome after {drug}'][outcome] += 1

    # sort it (reference/pointer sorting)
    if len(statDict[f'Days between drug {drug} and First AE']) > 1:
        u = zip(
            statDict[f'Days between drug {drug} and First AE'],
            statDict[f'Days between drug {drug} and First AE IDs'])
        u = sorted(u, key=itemgetter(0), reverse=False)
        statDict[f'Days between drug {drug} and First AE'], statDict[
            f'Days between drug {drug} and First AE IDs'] = zip(*u)

    return statDict

def getDrugStatsToTargetAE(eD, drugColName, drugOrIngredient, target, targetIsGroup, exactM=True, filters=None):
    drug = drugOrIngredient
    filterData(eD, filters, printStatus=False)
    statDict = {}

    # Parent Filter
    statDict['Total Cases Under Parent Filter'] = len(eD.idDict)

    # Filter Remaining To Cases with Suspect & Interacting Drugs Only
    suspectIDs = set(eD.getMatches('Drugs', 'Role', 'Suspect'))
    interactingIDs = set(eD.getMatches('Drugs', 'Role', 'Interacting'))
    suspectAndInteractingIDs = suspectIDs.union(interactingIDs)
    eD.filterByIDs(suspectAndInteractingIDs)
    statDict['Cases with suspect and/or interacting drug(s)'] = len(eD.idDict)

    # Filter Remaining To Only Containing Drug Of Interest
    eD.filterIDs('Drugs', drugColName, drug, exact=exactM)
    statDict[f'Cases with suspect and/or interacting {drug} ({drugColName})'] = len(eD.idDict)
    statDict[f'Only {drug}'] = 0
    statDict[f'{drug} + 1 other drug'] = 0
    statDict[f'{drug} + >=2 other drugs'] = 0
    statDict[f'Max Dose of {drug} per Case ID (per Day)'] = {}
    statDict[f'Max Dose of {drug} per Case ID (per Day)']['Unknown'] = 0
    statDict[f'Target AE Outcome after {drug}'] = {}
    statDict[f'Days between drug {drug} and Target AE'] = []
    statDict[f'Days between drug {drug} and Target AE IDs'] = []
    statDict[f"Is target AE '{target}' MedDRA term"] = not targetIsGroup
    statDict[f"Is target AE '{target}' MedDRA group"] = targetIsGroup
    statDict[f"Number of Serious Cases under {drug}"] = 0

    for ID in eD.idDict:
        #print(ID)
        if str(eD.idDict[ID]['Cases']['Serious']).lower() == 'yes':
            statDict[f"Number of Serious Cases under {drug}"] += 1

        # Determine Number of Suspect Drugs Per Case ID
        numSuspect = eD.idDict[ID]['Custom']['Drugs: Total Suspect']
        numInteracting = eD.idDict[ID]['Custom']['Drugs: Total Interacting']
        numSI = numSuspect + numInteracting
        #if numInteracting > 0: print('nI ;', ID, numInteracting, numSI)
        if numSI == 1:
            statDict[f'Only {drug}'] += 1
        elif numSI == 2:
            statDict[f'{drug} + 1 other drug'] += 1
        elif numSI > 2:
            statDict[f'{drug} + >=2 other drugs'] += 1

        # Get Relevant Drug Indexes
        drugIdxs = getDrugIdxs(eD, ID, drugColName, drug)

        # Get Maximum Dosage Per Day For This ID
        maxDoseIdx = getMaxDoseIdx(eD, ID, drugIdxs)
        if pd.isnull(maxDoseIdx):
            statDict[f'Max Dose of {drug} per Case ID (per Day)']['Unknown'] += 1
        else:
            maxDosage = f"{eD.idDict[ID]['Custom']['Drug Dosage (Daily)'][maxDoseIdx]}"
            maxDosage += f" {eD.idDict[ID]['Drugs']['Dose unit'][maxDoseIdx]}"
            if maxDosage not in statDict[f'Max Dose of {drug} per Case ID (per Day)']:
                statDict[f'Max Dose of {drug} per Case ID (per Day)'][maxDosage] = 0
            statDict[f'Max Dose of {drug} per Case ID (per Day)'][maxDosage] += 1

        # Get Target AE Outcome
        outcome = None
        # Get First Administration of Drug
        earliestDrugIdx = getInitialDrugAdministrationIdx(eD, ID, drugIdxs)
        # Get Time to Target AE from First Administration of Drug
        timeToTargetAE = None
        # print(target, targetIsGroup)
        AEIdxs = getAEIdxsAfterDrugAdministration(eD, ID, earliestDrugIdx, target=target, targetIsGroup=targetIsGroup)

        # Supersede Death for All Outcomes as the only applicable outcome
        if 'Died' in eD.idDict[ID]['Reactions']['Outcome']:
            AEIdxs = [eD.idDict[ID]['Reactions']['Outcome'].index('Died')]

        for AEIdx in AEIdxs:
            if earliestDrugIdx == None: break
            time = eD.idDict[ID]['Custom']['Extra: (H) Drug-AE Matrix'][earliestDrugIdx][AEIdx]
            if timeToTargetAE == None:
                timeToTargetAE = time
                outcome = eD.idDict[ID]['Reactions']['Outcome'][AEIdx]
            else:
                if time < timeToTargetAE:
                    timeToTargetAE = time
                    outcome = eD.idDict[ID]['Reactions']['Outcome'][AEIdx]
        if not pd.isnull(timeToTargetAE) and timeToTargetAE >= 0:
            statDict[f'Days between drug {drug} and Target AE'].append(timeToTargetAE)
            statDict[f'Days between drug {drug} and Target AE IDs'].append(ID)
            if outcome not in statDict[f'Target AE Outcome after {drug}']:
                statDict[f'Target AE Outcome after {drug}'][outcome] = 0
            statDict[f'Target AE Outcome after {drug}'][outcome] += 1

    # sort it (reference/pointer sorting)
    # print(statDict[f'Days between drug {drug} and Target AE'])
    if len(statDict[f'Days between drug {drug} and Target AE']) > 1:
        u = zip(
            statDict[f'Days between drug {drug} and Target AE'],
            statDict[f'Days between drug {drug} and Target AE IDs'])
        u = sorted(u, key=itemgetter(0), reverse=False)
        statDict[f'Days between drug {drug} and Target AE'], statDict[
            f'Days between drug {drug} and Target AE IDs'] = zip(*u)

    return statDict

def getGroupAssociatedDrugStats(eD, drugColName, drugOrIngredient, exactM=True, filters=None, printStatus=True):
    target_drug = drugOrIngredient

    #print('GA')
    #print(len(eD.idDict))
    filterData(eD, filters, printStatus=printStatus)
    filterData(eD, [('Drugs', drugColName, target_drug, exactM)], printStatus=printStatus, regen = False)

    #print(len(eD.idDict))
    groupCounts = {}
    for ID in eD.idDict:
        groupNames = set(eD.idDict[ID]['Custom']['Reactions: Group'])
        for groupName in groupNames:
            if groupName not in groupCounts: groupCounts[groupName] = 0
            groupCounts[groupName] += 1

    return groupCounts

def getSuspectDrugs(eD, drugColName, filters=None, printStatus=False):
    filterData(eD, filters, printStatus=printStatus)

    suspectDrugCounts = {}

    for ID in eD.idDict:
        suspectSet = set()  # prevent double count
        for drugIdx in range(len(eD.idDict[ID]['Drugs']['Role'])):
            role = eD.idDict[ID]['Drugs']['Role'][drugIdx]
            if not eD.idDict[ID]['Drugs'][drugColName][drugIdx]: continue
            for name in eD.idDict[ID]['Drugs'][drugColName][drugIdx].split(';'):
                if role == 'Suspect': suspectSet.add(name)

        for name in suspectSet:
            if name not in suspectDrugCounts: suspectDrugCounts[name] = 0
            suspectDrugCounts[name] += 1

    return suspectDrugCounts


# PRINT METHODS
def printOneVarStats(eD, sheetName, colName):
    median, q1, q3, mean, stdev, mini, maxi, size, lowerCIb, upperCIb = getOneVarStats(eD, sheetName, colName)
    print(f'One Var Stats for [{sheetName}][{colName}] (# Of IDs with Available Data = {size}): ')
    print(f'\tMedian [IQR]: {median} [{q1}-{q3}]')
    print(f'\tMean (stdev): {mean} ({stdev})')
    print(f'\t95% CI: {lowerCIb}-{upperCIb}')
    print(f'\t[min-max]: {mini}-{maxi}')

def printSimpleGroupOverlaps(eD, sheetName, colName, filters=None, printStatus=False):
    print(f"Simple Group Overlap ID Counts for [{sheetName}][{colName}]: ")
    pp.pprint(getSimpleGroupOverlaps(eD, sheetName, colName, filters=filters, printStatus=printStatus))
    return

def printDrugStatDict(statDict):
    for key in statDict:
        #print(key)
        if 'Days between drug' not in key:
            print(key, end=': ')
            pp.pprint(statDict[key])
        else:
            print(key, end=': \n')
            print(statDict[key])
            data = statDict[key]
            if 'IDs' not in key and len(data) > 0:
                q1, median, q3 = ov.quartiles(data)
                mean = ov.mean(data)
                stdev = ov.stdev(data)
                print(f'\tMedian [IQR]: {median} [{q1}-{q3}]')
                print(f'\tMean (stdev): {mean} ({stdev})')
                print(f'\t[min-max]: {min(data)}-{max(data)}')
        #print('finish')

def printDrugStats(eD, colName, drugOrIngredient, exactM=True, filters=None):
    printDrugStatDict(getDrugStats(eD, colName, drugOrIngredient, exactM, filters))

def printDrugStatsToTargetAE(eD, drugColName, drugOrIngredient, target, targetIsGroup, exactM=True, filters=None):
    printDrugStatDict(getDrugStatsToTargetAE(eD, drugColName, drugOrIngredient, target, targetIsGroup, exactM=exactM,
                                             filters=filters))

def printGroupAssociatedDrugStats(eD, drugColName, drugOrIngredient, exactM=True, filters=None, printStatus=False):
    groupCounts = getGroupAssociatedDrugStats(eD, drugColName, drugOrIngredient, exactM, printStatus=printStatus)
    print(f"Group Associated Case ID counts under {drugColName} for {drugOrIngredient}, exactMatches = {exactM}:")
    for groupName in sorted(groupCounts.keys()):
        print(f"\t{groupName}: {groupCounts[groupName]}")

def printSuspectDrugCounts(eD, drugColName, filters=None, printStatus=False):
    suspectDrugCounts = getSuspectDrugs(eD, drugColName, filters=filters, printStatus=printStatus)

    print(f"Case ID Count for suspect drugs under {drugColName}:")
    for drug in sorted(suspectDrugCounts.keys()):
        print(f"\t{drug}: {suspectDrugCounts[drug]}")