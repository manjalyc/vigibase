#Only the 4 variables below need to be changed
inputCSVFile='overlap.csv' #Must Have Headers, the data types can be numbers or strings, regardless this program treats all ids as strings
outputCountsCSVFile='counts.csv' #OutputFile for the counts
outputMatchesCSVFile='matches.csv' #OutputFile for the matches
outputRFile='all-overlap-generated.R' #OutputFile for the R program

import itertools
from operator import itemgetter

dataSets = {}
nonUniqueIds = set()

#Returns a list containing matching strings in ordered list1 and list2
def compareOrderedLists(list1, list2):
    ret = []
    l2idx = 0
    for mid in list1:
        while l2idx < len(list2):
            if mid < list2[l2idx]: break
            elif mid == list2[l2idx]:
                ret.append(mid)
                break
            l2idx += 1
    return ret

#Takes a list of headers (according to the inputCSVFile) and returns a list of all matching strings under those headers
def getMatchingList(setNameList):
    activeList = dataSets[setNameList[0]]
    for i in range(1, len(setNameList)):
        activeList = compareOrderedLists(activeList, dataSets[setNameList[i]])
    return activeList

#Generates the Output CSVs
def genCSVs(countCSV, matchCSV, allSetNames):
    cC = open(countCSV, 'w')
    mC = open(matchCSV, 'w')
    for N in range(2, len(allSetNames)+1):
        #print(f"N = {N}")
        cC.write(f"N = {N}\n")
        mC.write(f"N = {N}\n")
        for iterable in itertools.combinations(allSetNames, N):
            setNameList = list(iterable)
            matches = getMatchingList(setNameList)
            #print(len(matches))
            #print(f"{setNameList} matches: {len(matches)} | {matches}")
            cC.write('|'.join(setNameList))
            cC.write(f",{len(matches)}\n")
            mC.write('|'.join(setNameList))
            mC.write(f",{','.join(matches)}\n")
    cC.close()
    mC.close()

#Maintain list of nonUnique Ids
def updateNonUnique(not_unique_ids):
	for nid in not_unique_ids: nonUniqueIds.add(nid)

#Get # of unique ids
def getNumberOfUniqueIds(header):
	count = 0
	for key in dataSets[header]:
		if key not in nonUniqueIds: count += 1
	return count

def getRformattedIntersectStr(intersectStr):
	intersectStr = intersectStr.replace('|','&')
	#intersectStr = intersectStr.replace(' ','_')
	#intersectStr = intersectStr.replace('(','.').replace(')','.')
	#intersectStr = intersectStr.replace('-','.')
	return intersectStr

def genRFile(RFile, countCSV):
	oR = open(RFile, 'w')
	#get data from countCSV File
	intersectStrs = []
	countStrs = []
	for line in open(countCSV,'r'):
		line=line.strip()
		if line.find("N = ") == 0: continue
		intersectStr, countStr = line.split(',')
		
		#format the strings
		countStr = int(countStr)
		intersectStr = getRformattedIntersectStr(intersectStr)
		
		if countStr == 0: continue
		intersectStrs.append(intersectStr)
		countStrs.append(countStr)
		#print(intersectStr, countStr)
	
	#sort both lists indexes by # of counts descending
	u = zip(intersectStrs, countStrs)
	u = sorted(u, key=itemgetter(1), reverse = True)
	intersectStrs, countStrs = zip(*u)
	
	#Write R File
	oR.write('# Specific library\nlibrary(UpSetR)\n\n')
	oR.write('# Dataset\ninput <- c(\n')
	
	
	"""
	for header in dataSets:
		#oR.write(f'  {getRformattedIntersectStr(header)} = {len(dataSets[header])},\n')
		oR.write(f'    "{getRformattedIntersectStr(header)}" = {getNumberOfUniqueIds(header)},\n')
	"""
	
	for i in range(len(intersectStrs)-1):
		oR.write(f'  "{intersectStrs[i]}" = {countStrs[i]},\n')
	oR.write(f'  "{intersectStrs[-1]}" = {countStrs[-1]}\n)\n\n')
	
	oR.write(f'# Plot\nupset(fromExpression(input), nintersects = 30, nsets = {len(dataSets)}, order.by = "freq", decreasing = T, mb.ratio = c(0.6, 0.4), number.angles = 0, text.scale = 1.1, point.size = 2.8, line.size = 1)\n')
	
	oR.close()




def main():
	#Read the Data from inputCSVFile into dataSets
	headers = None
	for line in open(inputCSVFile,'r'):
		line = line.strip()
		if not headers:
			headers = line.split(',')
			for i in range(len(headers)): dataSets[headers[i]] = []
			continue
		data=line.split(',')
		for i in range(len(data)):
			if data[i]: dataSets[headers[i]].append(data[i])
	
	#Sort the Data Sets and get a list of all headers
	allSetNames = []
	for setName in dataSets:
		dataSets[setName].sort()
		allSetNames.append(setName)

	#Generate the CSV files
	genCSVs(outputCountsCSVFile, outputMatchesCSVFile, allSetNames)
	genRFile(outputRFile, outputCountsCSVFile)



	"""
	for N in range(2, len(allSetNames)+1):
		print(f"N = {N}")
		for iterable in itertools.combinations(allSetNames, N):
			setNameList = list(iterable)
			matches = getMatchingList(setNameList)
			print(f"{setNameList} matches: {len(matches)} | {matches}")
	"""

if __name__ == '__main__':
    main()
